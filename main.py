#!/usr/bin/env python3
import math
import sys
import os
import random
import pickle

from multiprocessing import Process, Array, Value, Pool
from functools import partial
from copy import deepcopy

import numpy
import scipy.optimize
try:
    import pylab
    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d import axes3d
    from matplotlib import cm
    from matplotlib import colors
except:
    pylab = None
# import nlopt

from simsimpy import other
from simsimpy.subset import SubsetStorage
import simsimpy as ssp

import cpgpy
import simulator
import cost_function
# import output
# import symmetrical
# import search_wrappers
# from optim import *
# from walk_search import *
import meta


import warnings

# warnings.filterwarnings('error')

################ Optimization #################################################
def point_in_bounds(bounds):
    return [random.uniform(i[0], i[1]) for i in bounds]


def get_periods(point, us, descrip):
    """Create a CPG, assign point to description, run on us."""
    raise DeprecationWarning()
    if point is None:
        return None

    cpg = cpgpy.Cpg_Analytical()
    sim = cpgpy.Simulator(cpg)
    sim.set_preset('zeros')
    sim.x_start = [0.]*4
    sim.set_point(point, descrip)

    sim.us = us
    sim.dt = 1e-9

    return sim.get_periods_overlaps_ladder_p()


def try_point(point, us=None, descrip=None, verbose=True):
    if us is None:
        us = cost_function.US()
    if descrip is None:
        descrip = symmetrical.DESCRIPTION
    # return cost_function.cost_function(get_periods(point, us, descrip),
    #                                    verbose=verbose)
    print(cost_function.cost_function(get_periods(point, us, descrip),
                                      verbose=verbose))
    output.plot_point(point, us, descrip, dir_output='result15')


class TryPoint(object):
    """docstring for TryPoint"""
    def __init__(self, descrip, active, cf, us,
                 bounds=None, dump=False, verbose=False, cgr=None):
        self.descrip = descrip
        self.verbose = verbose
        self.active = active
        self.cf = cf
        self.us = us
        self.bounds = bounds

        # self.cpg = cpgpy.Cpg_Analytical(solution_type='s')
        self.cpg = cpgpy.Cpg_Numerical()
        self.cpg.dt = 0.008
        self.ps = simulator.CpgParamSetter(descrip)
        self.cgr = cgr;

        self.demonstrate = False

        self.dump = dump
        if dump:
            self.points = []
            self.cf_vals = []

    def __call__(self, point, *args, **kwargs):
        if self.bounds is not None and not self.bounds(point):
            # print(point)
            # print(self.bounds(point))
            return cost_function.ERROR_MULTIPLIER * 10
        if self.descrip == 'symmetrical' and (
                abs(point[5]) < 1e-6 and abs(point[6]) < 1e-6):
            return cost_function.ERROR_MULTIPLIER * 10

        ts = [0]*len(self.us)
        periodss = [0]*len(self.us)
        overlapss = [0]*len(self.us)
        for i, u in enumerate(self.us):
            self.cpg.flush()
            self.cpg.active = list(self.active)
            self.ps(self.cpg, point)
            if self.cgr is not None:
                self.cgr(self.cpg)
            self.cpg.u = [u]*4
            ts[i], periodss[i], overlapss[i] = self.cpg.walk_full_step()

        cf = self.cf(periodss, overlapss)
        if self.demonstrate:
            err_distrib = [0]*4
            self.cf(periodss, overlapss, err_distrib=err_distrib)
            print('Error distrib:', err_distrib)

        if self.dump:
            self.points.append(point)
            self.cf_vals.append(cf)

        if self.verbose:
            print('{}\t{}'.format(
                cf, ' '.join(['{:.4f}'.format(i) for i in point])), end='\r')

        return cf


def find_optimal_symmetrical(restart=False, bruteforce=0,
                             filename='best_solution.pcl',
                             overwrite_kwargs={},
                             brute_filename='brute_solution.pcl',
                             filename_start=None):
    if restart:
        us = cost_function.US(N=meta.US_N)
        descrip = 'symmetrical'
        ps = simulator.CpgParamSetter(descrip)
        x0 = ps.x_start()
        optimizer_kwargs = {
            'niter': 1000,
            'T': 1,
            'stepsize': 1.,
            'interval': 30,
            'disp': True,
            'accept_test': other.Bounds(ps.bounds()),
            'niter_success': 60,
            }
    else:
        fload = filename_start if filename_start is not None else filename
        with open(fload, 'rb') as f:
            x0, y0, us, descrip, active, optimizer_kwargs = pickle.load(f)

    # x0[0] = 0.01
    # x0[1] = 0.99

    print('Starting from point:')
    print(x0)

    cf = cost_function.cost_function
    us = cost_function.US(N=meta.US_N)
    active = [True, False, False, True]

    for key, item in overwrite_kwargs.items():
        optimizer_kwargs[key] = item

    tp = TryPoint(descrip, active, cf, us,
                  bounds=other.Bounds(
                      simulator.CpgParamSetter(descrip).bounds()),
                  dump=True, verbose=True)

    if bruteforce:
        tp.verbose = False
        res = ssp.optimize.brute(
            tp, simulator.CpgParamSetter(descrip).bounds(), bruteforce,
            disp=True)
        print('Finished bruteforcing')
        print(res)
        x0 = res['x0']
        tp.verbose = True
        with open(brute_filename, 'wb') as f:
            pickle.dump(
            [x0, res['fval'], us, descrip, active, optimizer_kwargs], f)

    res = scipy.optimize.basinhopping(tp, x0, **optimizer_kwargs)
    print('Finished')
    print(res)
    savebestx = res['x']
    savebestcf = res['fun']

    print('\n\tFrom dump:')
    mincf = min(tp.cf_vals)
    minind = tp.cf_vals.index(mincf)
    minpoint = tp.points[minind]
    print('Minimum fval = {}'.format(mincf))
    print('At point')
    print(minpoint)
    print()

    if mincf < savebestcf:
        print('Better value in dump, not changing results')
        # savebestcf = mincf
        # savebestx = minpoint
    else:
        print('Dump does not have a better point')

    if filename_start is None or filename_start == filename:
        with open(filename, 'rb') as f:
            x0dash, *args = pickle.load(f)
        y0dash = tp(x0dash)
        if y0dash < savebestcf:
            print('Better value in file, will not overwrite:', y0dash)
            savebestcf = y0dash
            savebestx = x0dash
        else:
            print('File has worse value, overwriting:', y0dash)

    with open(filename, 'wb') as f:
        pickle.dump(
            [savebestx, savebestcf, us, descrip, active, optimizer_kwargs], f)

    if numpy.array_equal(savebestx, x0):
        print('Point did not move')
    else:
        print('Relevant vector changes:')
        for i, x in enumerate(savebestx):
            print('%2.8f\t' % (x-x0[i]), end=' ')
        print()


def plot_best(filename, dirname, an=True, num=False, point=None, cgr=None,
              plot_ruler=False, plot_periods=False, plot_speedvsinput=False,
              plot_resolution=None, plot_two_steps=False):
    with open(filename, 'rb') as f:
        x0, y0, us, descrip, active, optimizer_kwargs = pickle.load(f)

    us = cost_function.US(N=meta.US_N)
    # us = cost_function.US(N=6)
    # us = us[:1]

    if point is not None:
        x0 = point

    tp = TryPoint(descrip, active, cost_function.cost_function, us,
                  bounds=other.Bounds(
                      simulator.CpgParamSetter(descrip).bounds()),
                  cgr=cgr,
                  dump=True, verbose=True)
    print('tryin TryPoint:')
    tp.demonstrate = True
    tp(x0)
    print('\ndone')

    if an:
        simulator.show('analytical', descrip, x0, us,
                       plot_ruler=plot_ruler,
                       plot_periods=plot_periods,
                       plot_speedvsinput=plot_speedvsinput,
                       run_costfunction=True,
                       dirname=dirname,
                       active=active,
                       cgr=cgr,
                       plot_resolution=plot_resolution,
                       two_steps=plot_two_steps)
    if num:
        simulator.show('numerical', descrip, x0, us,
                       plot_ruler=plot_ruler,
                       plot_periods=plot_periods,
                       plot_speedvsinput=plot_speedvsinput,
                       run_costfunction=True,
                       dirname=dirname,
                       active=active,
                       cgr=cgr,
                       plot_resolution=plot_resolution,
                       two_steps=plot_two_steps)


def perturb_optimize_p(*args,
                       us=[], descrip='symemtrical',
                       active=[True, False, False, True], optimizer_kwargs={}):
    x0, denominator = args[0]

    print(denominator, 'Started with x:', x0)

    tp = TryPoint(descrip, active, cost_function.cost_function, us,
                  bounds=other.Bounds(
                      simulator.CpgParamSetter(descrip).bounds()),
                  dump=False, verbose=False)

    res = scipy.optimize.basinhopping(tp, x0, **optimizer_kwargs)
    print(denominator, 'Finished with y:', res['fun'])
    return res


def perturb_optimize(N, filename_o, filename, overwrite_kwargs, sd):
    with open(filename, 'rb') as f:
        x0, y0, us, descrip, active, optimizer_kwargs = pickle.load(f)

    us = cost_function.US(N=meta.US_N)

    for key, item in overwrite_kwargs.items():
        optimizer_kwargs[key] = item

    f = partial(perturb_optimize_p,
                us=list(us), descrip=descrip, active=list(active),
                optimizer_kwargs=deepcopy(optimizer_kwargs))

    x0s = []
    params = []
    ps = simulator.CpgParamSetter(descrip)
    for i in range(N):
        x0s.append(ps.near_optimal_point(sd=sd, mean=x0))
        params.append([x0s[-1], '\t{} {}/{}'.format(filename_o, i+1, N)])

    p = Pool(meta.pps)
    ress = p.map(f, params)

    xs = []
    ys = []
    for res in ress:
        xs.append(res['x'])
        ys.append(res['fun'])

    p.close()

    with open(filename_o, 'wb') as f:
        pickle.dump([xs, ys, ress, optimizer_kwargs], f)
    print('Generated and saved {} results to {}'.format(N, filename_o))


def arrs2meanssd(a):
    means = [numpy.mean(i) for i in a]
    stds = [numpy.std(i) for i in a]
    return (means, stds)


def compare_perturbations(filenames, methods, figname, cutoffqual=None):
    """cutoff will include cutoff portion of best values of y.

    E.g. cutoffqual of 0.1 would lead to 10% of the best to be shown"""
    xss = []
    yss = []
    yss_flattened = []
    for filename in filenames:
        with open(filename, 'rb') as f:
            xs, ys, ress, optimizer_kwargs = pickle.load(f)
        xss.append(xs)
        yss.append(ys)
        yss_flattened += ys
    yss_flattened.sort()
    # print(yss_flattened)
    if cutoffqual is None:
        cutoff_y = max(yss_flattened)
    else:
        cutoff_y = yss_flattened[int(len(yss_flattened)*cutoffqual)]
    print('Cutting off at', cutoff_y)

    # Drop out shit
    for i in range(len(yss)):
        best_y_l = numpy.inf
        best_x_l = []
        for j in range(len(yss[i])-1, -1, -1):
            if yss[i][j] < best_y_l:
                best_y_l = yss[i][j]
                best_x_l = xss[i][j]
            if yss[i][j] > cutoff_y:
                # print('Dropping', yss[i][j])
                yss[i].pop(j)
                xss[i].pop(j)
        if not len(yss[i]):
            print('Deleted too much, returning one best')
            yss[i].append(best_y_l)
            xss[i].append(best_x_l)
        print('Left', len(yss[i]), 'elements')

    bounds = simulator.CpgParamSetter('symmetrical').bounds()

    dim = len(xss[0][0])
    descs = ['x1f', 'x4e', 'rfe', 'ref', 'ree', 'guf', 'gue', 'leakf', 'leake',
             'u0f', 'u0e']

    pylab.figure(figsize=(32, 6))
    pylab.suptitle('Variability of parameter estimation after perturbation and '
                   'convergence using various minimization methods')

    ax = pylab.subplot(1, dim+1, 1)
    pylab.title('Cost function')

    means, stds = arrs2meanssd(yss)
    x = list(range(len(means)))
    width = 0.6
    xdash = [j-width/2 for j in x]
    # print(xdash)
    pylab.bar(xdash, means, width, yerr=stds)
    # pylab.errorbar(x, means, yerr=stds, fmt='.', color='k')
    pylab.xticks(x, methods)

    for i, desc in enumerate(descs):
        ax = pylab.subplot(1, dim+1, i+2)
        pylab.title(desc)

        means, stds = arrs2meanssd([[j[i] for j in k] for k in xss])
        x = list(range(len(means)))
        pylab.bar(xdash, means, width, yerr=stds)
        # pylab.errorbar(x, means, yerr=stds, fmt='.', color='k')
        pylab.xticks(x, methods)
        pylab.ylim(bounds[i])

    pylab.savefig(figname)


def identify_best(filenames, methods, best_filename, idbest=1):
    descrip = 'symmetrical'
    active = [True, False, False, True]
    us = cost_function.US(N=meta.US_N)
    tp = TryPoint(descrip, active,
                  cost_function.cost_function,
                  us,
                  bounds=other.Bounds(
                      simulator.CpgParamSetter(descrip).bounds()),
                  dump=False, verbose=False)

    with open(best_filename, 'rb') as f:
        best_x, _, _, _, _, optimizer_kwargs = pickle.load(f)
        best_y = tp(best_x)
        best_ok = optimizer_kwargs

    print('\tOriginal best:')
    print(best_x)
    print(best_y)
    print(best_ok)

    for filename in filenames:
        with open(filename, 'rb') as f:
            xs, _, _, optimizer_kwargs = pickle.load(f)
            for x in xs:
                y = tp(x)
                if y < best_y:
                    best_y = y
                    best_x = x
                    best_ok = optimizer_kwargs

    print('\tThe best value found:')
    print(best_x)
    print(best_y)
    print(best_ok)
    if idbest == 2:
        print('\tSaving')

        with open(best_filename, 'wb') as f:
            pickle.dump(
                [best_x, best_y, us, descrip, active, best_ok], f)
    else:
        print('\tNot saving')


def uncouple(filename, N, desc, maxper, decoupler, fname_o):
    with open(filename, 'rb') as f:
        vector, best_y, us, descrip, active, best_ok = pickle.load(f)

    axpers = numpy.linspace(1-maxper, 1+maxper, N)
    ax1 = [0]*N
    ax2 = [0]*N
    err_distribs = []

    cpg = cpgpy.Cpg_Analytical()
    ps = simulator.CpgParamSetter(descrip)

    for i1, ax1per in enumerate(axpers):
        err_distribs.append([])
        for i2, ax2per in enumerate(axpers):
            ts = [0]*len(us)
            periodss = [0]*len(us)
            overlapss = [0]*len(us)
            for i, u in enumerate(us):
                cpg.flush()
                cpg.active = list(active)
                ps(cpg, vector)
                buf = decoupler(cpg, vector, ax1per, ax2per)
                cpg.u = [u]*4
                ts[i], periodss[i], overlapss[i] = cpg.walk_full_step()
            ax1[i1] = buf[0]
            ax2[i2] = buf[1]

            err_distrib = [0]*4
            cf = cost_function.cost_function(periodss, overlapss,
                                             err_distrib=err_distrib)
            err_distribs[-1].append(err_distrib)

            # left_mod = 0.78
            # right_mod = 0.9

            # left_mod = 0.726
            # right_mod = 1
            # if buf[0] > 0.902 and buf[1] > 0.845:
            # if i1 == 5-1 and i2 == 11-1:
            #     print(us)
            #     print(buf)
            #     print(err_distrib)
            #     print(cf)
            #     sys.exit()
            # if abs(ax2[i2] - 0.34991763488108968) < 1e-6 and abs(ax1[i1] - 0.47830537420574626) < 1e-6:
            #     print('\n', ax1[i1], ax2[i2], err_distrib, '\n')

    # fname_o = filename[:-4]+'_variate_'+desc+filename[-4:]
    # print([''.join(['{:>5.2}'.format(j[-1]) for j in i]) for i in err_distribs])
    with open(fname_o, 'wb') as f:
        pickle.dump([ax1, ax2, err_distribs], f)


def plot_uncoupled(filename, desc, plot_full=False):
    with open(filename, 'rb') as f:
        ax1, ax2, err_distribs = pickle.load(f)
    plot_full = True

    dmesh1, dmesh2 = numpy.meshgrid(ax1, ax2)
    if plot_full:
        fig = pylab.figure()
    else:
        fig = pylab.figure(figsize=(11, 4))
    pylab.suptitle(desc)

    if plot_full:
        pylab.subplot(3, 2, 1)
    else:
        pylab.subplot(1, 2, 1)
        pylab.title('left speed / right speed')
    buf = [[j[-1] for j in i] for i in err_distribs]
    # print(buf)

    vmin = 0.8
    vmax = 1.2

    norm = colors.Normalize(vmin=vmin, vmax=vmax, clip=True)
    pylab.pcolormesh(dmesh1, dmesh2, buf,  # levels=levels,
                   cmap='coolwarm', norm=norm)
    pylab.colorbar(ticks=[vmin, 1., vmax], extend='both',
                   label='left speed / right speed')
    # cb.set_xticks([0.8, 1., 1.2])

    # pylab.plot(0.897, 0.759, 'k*')
    # pylab.plot(0.897, 0.736, 'k*')

    pylab.xlabel(desc)
    pylab.ylabel(desc)

    if plot_full:
        pylab.subplot(3, 2, 2)
        buf = [[j[0] for j in i] for i in err_distribs]
        pylab.pcolormesh(dmesh1, dmesh2, buf,  # levels=levels,
                       cmap=cm.coolwarm)
        # pylab.plot(0.897, 0.759, 'k*')
        # pylab.plot(0.897, 0.736, 'k*')
        pylab.colorbar().set_label('minmax')
        pylab.xlabel(desc)
        pylab.ylabel(desc)

        pylab.subplot(3, 2, 3)
        buf = [[j[1] for j in i] for i in err_distribs]
        pylab.pcolormesh(dmesh1, dmesh2, buf,  # levels=levels,
                       cmap=cm.coolwarm)
        # pylab.plot(0.897, 0.759, 'k*')
        # pylab.plot(0.897, 0.736, 'k*')
        pylab.colorbar().set_label('bio')
        pylab.xlabel(desc)
        pylab.ylabel(desc)

        pylab.subplot(3, 2, 4)
        buf = [[j[2] for j in i] for i in err_distribs]
        pylab.pcolormesh(dmesh1, dmesh2, buf,  # levels=levels,
                       cmap=cm.coolwarm)
        # pylab.plot(0.897, 0.759, 'k*')
        # pylab.plot(0.897, 0.736, 'k*')
        pylab.colorbar().set_label('ffee')
        pylab.xlabel(desc)
        pylab.ylabel(desc)

        pylab.subplot(3, 2, 5)
        buf = [[j[3] for j in i] for i in err_distribs]
        pylab.pcolormesh(dmesh1, dmesh2, buf,  # levels=levels,
                       cmap=cm.coolwarm)
        # pylab.plot(0.897, 0.759, 'k*')
        # pylab.plot(0.897, 0.736, 'k*')
        pylab.colorbar().set_label('lrspeeddiff')
        pylab.xlabel(desc)
        pylab.ylabel(desc)

    if plot_full:
        pylab.subplot(3, 2, 6)
    else:
        pylab.subplot(1, 2, 2)
        pylab.title('ffee+bio')
    buf = [[j[1]+j[2]*0.5 for j in i] for i in err_distribs]
    norm = colors.Normalize(vmin=0, vmax=0.1, clip=True)
    pylab.pcolormesh(dmesh1, dmesh2, buf,  # levels=levels,
                   cmap='YlOrBr', norm=norm)
    pylab.colorbar(ticks=[0, 0.05, 0.1, 0.2], extend='max', label='ffee+bio')

    # pylab.plot(0.897, 0.759, 'k*')
    # pylab.plot(0.897, 0.736, 'k*')

    pylab.xlabel(desc)
    pylab.ylabel(desc)

    pylab.savefig(filename[:-4]+'.png')
    # fig.subplots_adjust(wspace=0)




def variate_values_run(l, point, variation=0.1):
    print('Running {}'.format(symmetrical.DESCRIPTION[l]))
    param_range = [symmetrical.BOUNDS[l][0] * symmetrical.SCALE[l],
                   symmetrical.BOUNDS[l][1] * symmetrical.SCALE[l]]
    dls = numpy.linspace(max(param_range[0], point[l]*variation),
                         min(param_range[1], point[l]*(1.+variation)), 10)

    point_l = list(point)
    error_distributions = []
    for dl in dls:
        point_l[l] = dl
        error_distributions.append([0.]*4)
        cost_function.cost_function(
            get_periods(point_l,
                        cost_function.US(), symmetrical.DESCRIPTION),
            verbose=False, err_distrib=error_distributions[-1])

    pylab.figure(**meta.FIGARGS)
    for i in range(4):
        pylab.plot(dls, [_[0] for _ in error_distributions],
                   label=cost_function.ERROR_CALCULATIONS[i])
    pylab.plot(dls, [sum(_)/4. for _ in error_distributions], label='total')
    pylab.title('Variation of {}'.format(symmetrical.DESCRIPTION[l]))
    pylab.xlabel('{}'.format(symmetrical.DESCRIPTION[l]))
    pylab.ylabel('Errors')
    pylab.legend()
    pylab.savefig(os.path.join(
        'variate', '{}.png'.format(symmetrical.DESCRIPTION[l])))


def variate_values(point=None, dirname=None):
    if point is None:
        point = symmetrical.TEST_POINT
        if dirname is not None:
            cb = output.Callback()
            cb.verbose = False
            cb.add_file(os.path.join(dirname, 'final.pcl'))
            point = cb.best_result()
    for i in range(len(point)):
        variate_values_run(i, point)


def variate_value(l, point=None, dirname=None, variation=0.1):
    if point is None:
        point = symmetrical.TEST_POINT
        if dirname is not None:
            cb = output.Callback()
            cb.verbose = False
            cb.add_file(os.path.join(dirname, 'final.pcl'))
            point = cb.best_result()

    dls = numpy.linspace(max(param_range[0], point[l]*variation),
                         min(param_range[1], point[l]*(1.+variation)), 6)
    us = cost_function.US()

    pylab.figure(**meta.FIGARGS)
    dt = 0.001
    ts = list(other.dxrange(0, 10., dt))
    for dl in dls:
        point[l] = dl
        sim = cpg.Simulator(cpg.Cpg_Analytical())
        sim.dt = dt
        sim.set_point(point, symmetrical.DESCRIPTION)

        x = []


def test16():
    """Test nearby points if they are lower than this solution"""
    cb = output.Callback()
    cb.verbose = False
    cb.load_storage(fname=os.path.join('result7', 'final.pcl'))
    cb.sort(reverse=False)

    x0 = cb.storage[0][0]
    target = lambda x: try_point(x)
    directions, res = test_nearby_points(
        target, x0, 0.001, scale=symmetrical.SCALE, diagonal=False)

    for s in res.keys():
        print('{}: {}'.format(s, res[s]))

    point_value = res[str([0]*len(x0))]
    print('Start point value', point_value)
    for s in res.keys():
        if res[s] < point_value:
            print('Better point {} with value {}'.format(s, res[s]))


def test22():
    """paper fourth figure wrap"""
    output.paper_fourth_figure('result14', get_periods, nus=25, nbest=0)


def variate_lr(descrip, point, l1, l2, variation=0.1, npoints=9):
    dvals1 = numpy.linspace(point[l1]*(1. - variation),
                            point[l1]*(1. + variation), npoints)
    dvals2 = numpy.linspace(point[l2]*(1. - variation),
                            point[l2]*(1. + variation), npoints)

    answ = []
    full = []
    point_l = list(point)
    for dval1 in dvals1:
        answ.append([])
        full.append([])
        point_l[l1] = dval1
        for dval2 in dvals2:
            point_l[l2] = dval2
            error_distribution = [0.]*4
            cost_function.cost_function(
                get_periods(point_l, cost_function.US(), descrip),
                verbose=False, err_distrib=error_distribution)
            # error_distribution = [random.uniform(1, 100)]*4
            answ[-1].append(error_distribution[-1])
            full[-1].append(error_distribution)
    return [dvals1, dvals2, answ, full]


def test23preset1():
    descrip = [
        'x_start[0]', 'x_start[3]',
        'cpg.r[0][3]',  'cpg.r[2][1]',  # CHANGING
        'cpg.r[1][2], cpg.r[3][0]',
        'cpg.r[0][2], cpg.r[2][0]',
        'cpg.g_u[0], cpg.g_u[2]', 'cpg.g_u[1], cpg.g_u[3]',
        'cpg.leak[0], cpg.leak[2]', 'cpg.leak[1], cpg.leak[3]',
        'cpg.ci[0], cpg.ci[2]', 'cpg.ci[1], cpg.ci[3]']
    point = [
        0.4352, 0.3185,
        -2.1359, -2.1359,
        2.0618,
        0.0344,
        2.9938, 1.5562,
        1.164, -0.5041,
        0.0383, 2.279]
    l1 = 2
    l2 = 3
    return [descrip, point, l1, l2]


def test23preset2():
    descrip = [
        'x_start[0]', 'x_start[3]',
        'cpg.r[0][3], cpg.r[2][1]',
        'cpg.r[1][2]', 'cpg.r[3][0]',  # CHANGING
        'cpg.r[0][2], cpg.r[2][0]',
        'cpg.g_u[0], cpg.g_u[2]', 'cpg.g_u[1], cpg.g_u[3]',
        'cpg.leak[0], cpg.leak[2]', 'cpg.leak[1], cpg.leak[3]',
        'cpg.ci[0], cpg.ci[2]', 'cpg.ci[1], cpg.ci[3]']
    point = [
        0.4352, 0.3185,
        -2.1359,
        2.0618, 2.0618,
        0.0344,
        2.9938, 1.5562,
        1.164, -0.5041,
        0.0383, 2.279]
    l1 = 3
    l2 = 4
    return [descrip, point, l1, l2]


def test23preset3():
    descrip = [
        'x_start[0]', 'x_start[3]',
        'cpg.r[0][3], cpg.r[2][1]',
        'cpg.r[1][2], cpg.r[3][0]',
        'cpg.r[0][2]', 'cpg.r[2][0]',  # CHANGING
        'cpg.g_u[0], cpg.g_u[2]', 'cpg.g_u[1], cpg.g_u[3]',
        'cpg.leak[0], cpg.leak[2]', 'cpg.leak[1], cpg.leak[3]',
        'cpg.ci[0], cpg.ci[2]', 'cpg.ci[1], cpg.ci[3]']
    point = [
        0.4352, 0.3185,
        -2.1359,
        2.0618,
        0.0634, 0.0634,
        2.9938, 1.5562,
        1.164, -0.5041,
        0.0383, 2.279]
    l1 = 4
    l2 = 5
    return [descrip, point, l1, l2]
test23preset = test23preset3


def test23():
    descrip, point, l1, l2 = test23preset()

    dvals1, dvals2, answ, full = variate_lr(descrip, point, l1, l2,
                                            variation=0.3)

    fig = pylab.figure()

    ax = fig.add_subplot(1, 2, 1, projection='3d')
    dmesh1, dmesh2 = numpy.meshgrid(dvals1, dvals2)
    answnp = numpy.matrix(answ)
    ax.plot_wireframe(dmesh1, dmesh2, answ, cmap=cm.coolwarm)
    ax.contour(dmesh1, dmesh2, answ,
               zdir='z', offset=answnp.min(), cmap=cm.coolwarm)
    # ax.contour(dmesh1, dmesh2, answ,
    #                   zdir='x', offset=max(dvals2), cmap=cm.coolwarm)
    # ax.contour(dmesh1, dmesh2, answ,
    #                   zdir='y', offset=max(dvals1), cmap=cm.coolwarm)
    # ax.zlim((answnp.min(), answnp.max()))
    ax.set_xlabel(descrip[l1])
    ax.set_ylabel(descrip[l2])
    ax.set_zlabel('left speed / right speed')

    ax = fig.add_subplot(1, 2, 2,
                         projection='3d',
                         sharex=ax, sharey=ax
                         )
    dmesh1, dmesh2 = numpy.meshgrid(dvals1, dvals2)
    answ2 = [[0.5*i[0]+4.*i[1]+0.04*i[2] for i in j] for j in full]
    answ3 = numpy.matrix(answ2)
    ax.plot_wireframe(dmesh1, dmesh2, answ2, cmap=cm.coolwarm)
    ax.contour(dmesh1, dmesh2, answ2,
               zdir='z', offset=0., cmap=cm.coolwarm)
    ax.set_xlabel(descrip[l1])
    ax.set_ylabel(descrip[l2])
    ax.set_zlabel('total_error')
    pylab.savefig(
        os.path.join('smart_variate2', descrip[l1] + descrip[l2] + 'tot.png'))
    fig.subplots_adjust(wspace=0)

    pylab.show()


def test24():
    """2d version of test23"""
    descrip, point, l1, l2 = test23preset()

    dvals1, dvals2, answ, full = variate_lr(descrip, point, l1, l2,
                                            variation=0.3, npoints=9)

    fig = pylab.figure()

    pylab.subplot(1, 2, 1)
    dmesh1, dmesh2 = numpy.meshgrid(dvals1, dvals2)
    pylab.contourf(dmesh1, dmesh2, answ,  # levels=levels,
                   cmap=cm.coolwarm)
    pylab.colorbar().set_label('left speed / right speed')
    pylab.xlabel(descrip[l1])
    pylab.ylabel(descrip[l2])
    # ax.set_zlabel('left speed / right speed')

    pylab.subplot(1, 2, 2)
    # answ2 = [[i[0]+i[1]+i[2] for i in j] for j in full]
    answ2 = [[0.5*i[0]+4.*i[1]+0.04*i[2] for i in j] for j in full]
    pylab.contourf(dmesh1, dmesh2, answ2,  # levels=levels,
                   cmap=cm.coolwarm)
    pylab.colorbar().set_label('total_error')
    pylab.xlabel(descrip[l1])
    pylab.ylabel(descrip[l2])
    # ax.set_zlabel('total_error')
    pylab.savefig(
        os.path.join('smart_variate4', descrip[l1] + descrip[l2] + 'tot.png'))
    # fig.subplots_adjust(wspace=0)

    pylab.show()


def test25():
    """2d version of test23 with separate for each error"""
    descrip, point, l1, l2 = test23preset()

    dvals1, dvals2, answ, full = variate_lr(descrip, point, l1, l2,
                                            variation=0.3, npoints=9)

    fig = pylab.figure()

    pylab.subplot(2, 2, 1)
    dmesh1, dmesh2 = numpy.meshgrid(dvals1, dvals2)
    pylab.contourf(dmesh1, dmesh2, answ,  # levels=levels,
                   cmap=cm.coolwarm)
    pylab.colorbar().set_label('left speed / right speed')
    pylab.xlabel(descrip[l1])
    pylab.ylabel(descrip[l2])

    pylab.subplot(2, 2, 2)
    answ2 = [[i[0] for i in j] for j in full]
    pylab.contourf(dmesh1, dmesh2, answ2,  # levels=levels,
                   cmap=cm.coolwarm)
    pylab.colorbar().set_label('minmax')
    pylab.xlabel(descrip[l1])
    pylab.ylabel(descrip[l2])

    pylab.subplot(2, 2, 3)
    answ2 = [[i[1] for i in j] for j in full]
    pylab.contourf(dmesh1, dmesh2, answ2,  # levels=levels,
                   cmap=cm.coolwarm)
    pylab.colorbar().set_label('bio')
    pylab.xlabel(descrip[l1])
    pylab.ylabel(descrip[l2])

    pylab.subplot(2, 2, 4)
    answ2 = [[i[2] for i in j] for j in full]
    pylab.contourf(dmesh1, dmesh2, answ2,  # levels=levels,
                   cmap=cm.coolwarm)
    pylab.colorbar().set_label('ffee')
    pylab.xlabel(descrip[l1])
    pylab.ylabel(descrip[l2])

    pylab.savefig(
        os.path.join('smart_variate3', descrip[l1] + descrip[l2] + 'tot4.png'))
    # fig.subplots_adjust(wspace=0)

    pylab.show()


def decouple_ref(cpg, vector, ax1per, ax2per):
    if vector is not None:
        val = vector[2]
    else:
        val = cpg.r[0][3]
    cpg.r[0][3] = val*ax1per
    cpg.r[2][1] = val*ax2per
    return (cpg.r[0][3], cpg.r[2][1])


def decouple_rfe(cpg, vector, ax1per, ax2per):
    if vector is not None:
        val = vector[3]
    else:
        val = cpg.r[1][2]
    cpg.r[1][2] = val*ax1per
    cpg.r[3][0] = val*ax2per
    return (cpg.r[1][2], cpg.r[3][0])


def decouple_ree(cpg, vector, ax1per, ax2per):
    if vector is not None:
        val = vector[4]
    else:
        val = cpg.r[1][3]
    # val = 1
    cpg.r[1][3] = val*ax1per
    cpg.r[3][1] = val*ax2per
    return (cpg.r[1][3], cpg.r[3][1])


def decouple_guf(cpg, vector, ax1per, ax2per):
    if vector is not None:
        val = vector[5]
    else:
        val = cpg.g_u[0]
    cpg.g_u[0] = val*ax1per
    cpg.g_u[2] = val*ax2per
    return (cpg.g_u[0], cpg.g_u[2])


def decouple_gue(cpg, vector, ax1per, ax2per):
    if vector is not None:
        val = vector[6]
    else:
        val = cpg.g_u[1]
    cpg.g_u[1] = val*ax1per
    cpg.g_u[3] = val*ax2per
    return (cpg.g_u[1], cpg.g_u[3])


def decouple_gu(cpg, vector, ax1per, ax2per):
    if vector is not None:
        val = vector[6]
    else:
        val = cpg.g_u[1]
    cpg.g_u[1] = val*ax1per
    cpg.g_u[3] = val*ax2per
    if vector is not None:
        val = vector[5]
    else:
        val = cpg.g_u[0]
    cpg.g_u[0] = val*ax1per
    cpg.g_u[2] = val*ax2per
    return (ax1per, ax2per)


decoupling_functions = {
    'ref': decouple_ref,
    'rfe': decouple_rfe,
    'ree': decouple_ree,
    'guf': decouple_guf,
    'gue': decouple_gue,
    'gu': decouple_gu,
}


if __name__ == '__main__':
    """Command line arguments:
    -optimize <0,1> - optimize the symmetrical cpg, specify parameters in the
        function call
    -minimizer <name> - use specific minimizer for parameter search. Available
        options:
            cobyla
            lbfgsb
            powell
            tnc
    -maxfev <N> - maximum amount of function evaluations during search
    -precision <f> - precision of optimization
    -eps <f> - precision of gradient approximation (where applicable)
    -niter <N> - amount of basinhopping iterations
    -perturb <N> - amount of perturbed from optimal points to search from for
        new best solution. If no minimizer is specified, will go through all
        available in alphabetical order.
    -perturbsd <f> - standard deviation for perturbed points, defaults to 5%
    -plotperturb <f> - plots comparison between found best points during
        perturbations. Only best f portion will be used
    -identify_best <0,1,2> - finds the best point out of all perturbed results,
        If set to 2, saves result into the best solution file
    -uncouple <N> - uncouples connections within the cpg and varies them to
        measure changes in cost function and assimitricity of gait. NxN heatmap
        will be created.
    -uncoupled_plot <0,1> - plots the result currently stored in ucoupled files.
    -plot <0,1> - plots a few figures of the best solution
    -test_asym <>
    -test_asym_type <>
    -Usn <>
    """
    dirname = 'result'
    if not os.path.exists(dirname):
        os.mkdir(dirname)

    if len(sys.argv) > 1:
        argdict = dict(zip(sys.argv[1::2], sys.argv[2::2]))
    else:
        argdict = {}

    filename_short = (argdict['-filename']
                      if '-filename' in list(argdict.keys()) else
                      'best_solution.pcl')
    filename_start = (argdict['-filename_start']
                      if '-filename_start' in list(argdict.keys()) else
                      'best_solution.pcl')

    filename = os.path.join(dirname, filename_short)
    filename_start = os.path.join(dirname, filename_start)
    brute_filename = os.path.join(dirname, 'brute_solution.pcl')

    print(argdict)
    print('Start Dirname: {}, filename: {}, Pid: {}, Time: {}'.format(
        dirname, filename, os.getpid(), other.time_stamp()))

    if '-Usn' in list(argdict.keys()):
        meta.US_N = int(argdict['-Usn'])
    if '-Umin' in list(argdict.keys()):
        cost_function.HALBERTSMA_MIN_SPEED = float(argdict['-Umin'])
    if '-Umax' in list(argdict.keys()):
        cost_function.HALBERTSMA_MAX_SPEED = float(argdict['-Umax'])

    pbkwargs = {}
    if '-pr' in list(argdict.keys()):
        pbkwargs['plot_ruler'] = int(argdict['-pr'])
    if '-plot_ruler' in list(argdict.keys()):
        pbkwargs['plot_ruler'] = int(argdict['-plot_ruler'])
    if '-pp' in list(argdict.keys()):
        pbkwargs['plot_periods'] = int(argdict['-pp'])
    if '-plot_periods' in list(argdict.keys()):
        pbkwargs['plot_periods'] = int(argdict['-plot_periods'])
    if '-psvi' in list(argdict.keys()):
        pbkwargs['plot_speedvsinput'] = int(argdict['-psvi'])
    if '-plot_speedvsinput' in list(argdict.keys()):
        pbkwargs['plot_speedvsinput'] = int(argdict['-plot_speedvsinput'])
    if '-plot_resolution' in list(argdict.keys()):
        pbkwargs['plot_resolution'] = float(argdict['-plot_resolution'])
    if '-plot_two_steps' in list(argdict.keys()):
        pbkwargs['plot_two_steps'] = int(argdict['-plot_two_steps'])

    maxfev = (int(argdict['-maxfev'])
              if '-maxfev' in list(argdict.keys())
              else None)
    precision = (float(argdict['-precision'])
                 if '-precision' in list(argdict.keys())
                 else 1e-2)
    eps = (float(argdict['-eps'])
                 if '-eps' in list(argdict.keys())
                 else 1e-2)
    perturbsd = (float(argdict['-perturbsd'])
                 if '-perturbsd' in list(argdict.keys())
                 else 0.05)
    cobylaargs = {
        'method': 'COBYLA',
        'constraints': {
            'type': 'ineq',
            'fun': other.Bounds(
                simulator.CpgParamSetter('symmetrical').bounds())
        },
        'options': {
            'tol': precision,
            'maxiter': maxfev if maxfev is not None else 1000,
        }
    }
    powellargs = {
        'method': 'Powell',
        'options': {
            'ftol': precision,
            'xtol': precision,
            'maxfev': maxfev,
        }
    }
    tncargs = {
        'method': 'TNC',
        'bounds': simulator.CpgParamSetter('symmetrical').bounds(),
        'options': {
            'ftol': precision,
            'xtol': precision,
            'eps': eps,
            'maxiter': maxfev,
        }
    }
    lbfgsbargs = {
        'method': 'L-BFGS-B',
        'bounds': simulator.CpgParamSetter('symmetrical').bounds(),
        'options': {
            'ftol': precision,
            'eps': eps,
            'maxfun': maxfev if maxfev is not None else 15000,
        }
    }
    optargdict = {
        'cobyla': cobylaargs,
        'lbfgsb': lbfgsbargs,
        'powell': powellargs,
        'tnc': tncargs,
    }
    minimizer_specified = '-minimizer' in list(argdict.keys())
    minimizer_str = argdict['-minimizer'] if minimizer_specified else 'tnc'
    optarg = optargdict[minimizer_str]

    overwrite_kwargs = {
        'niter': int(argdict['-niter']) if '-niter' in list(argdict.keys()) else 200,
        'T': cost_function.ERROR_MULTIPLIER,
        'stepsize': 1,
        'interval': 30,
        'disp': True,
        'niter_success': None,
        'minimizer_kwargs': optarg
    }

    # OPTIMIZE
    bruteforce = int(argdict['-bruteforce']) if '-bruteforce' in list(argdict.keys()) else 0
    if '-optimize' in list(argdict.keys()) and int(argdict['-optimize']):
        find_optimal_symmetrical(
            restart=False,
            bruteforce=bruteforce,
            filename=filename,
            overwrite_kwargs=overwrite_kwargs,
            brute_filename=brute_filename,
            filename_start=filename_start)

    # PERTURB
    methods = sorted(list(optargdict.keys()))
    perturb_filenames = [os.path.join(
        dirname, 'perturb_result_{}.pcl'.format(method)) for method in methods]
    if '-perturb' in list(argdict.keys()) and int(argdict['-perturb']):
        if minimizer_specified:
            perturb_filename = os.path.join(
                dirname, 'perturb_result_{}.pcl'.format(minimizer_str))
            perturb_optimize(int(argdict['-perturb']),
                             perturb_filename,
                             filename,
                             overwrite_kwargs,
                             perturbsd)
        else:
            for method, perturb_filename in zip(methods, perturb_filenames):
                print('Doing method', method)
                overwrite_kwargs['minimizer_kwargs'] = optargdict[method]
                perturb_optimize(int(argdict['-perturb']),
                                 perturb_filename,
                                 filename,
                                 overwrite_kwargs,
                                 perturbsd)

    if '-plotperturb' in list(argdict.keys()) and float(argdict['-plotperturb']):
        compare_perturbations(
            perturb_filenames, methods,
            os.path.join(dirname, 'compare_perturbations.png'),
            cutoffqual=float(argdict['-plotperturb'])
            )

    if '-identify_best' in list(argdict.keys()) and int(argdict['-identify_best']):
        identify_best(perturb_filenames, methods, filename,
                      idbest=int(argdict['-identify_best']))

    # UNCOUPLE
    uncouple_fnames = dict((desc, filename[:-4]+'_variate_'+desc+filename[-4:])
                           for desc in decoupling_functions.keys())
    if '-uncouple' in list(argdict.keys()):
        try:
            buf = int(argdict['-uncouple'])
            for dcs in decoupling_functions.items():
                # if not dcs[0] == 'gu':
                #     continue
                uncouple(filename, buf, dcs[0], 0.333, dcs[1],
                         uncouple_fnames[dcs[0]])
        except ValueError:
            desc = argdict['-uncouple']
            uncouple(filename, int(argdict['-uncoupleN']), desc, 0.333,
                     decoupling_functions[desc],
                     uncouple_fnames[desc])

    if '-uncoupled_plot' in list(argdict.keys()):
        try:
            buf = int(argdict['-uncoupled_plot'])
            for desc in decoupling_functions.keys():
                plot_uncoupled(uncouple_fnames[desc], desc, plot_full=False)
        except ValueError:
            desc = argdict['-uncoupled_plot']
            plot_uncoupled(uncouple_fnames[desc], desc, plot_full=False)
        # pylab.show()
        # sys.exit()

    if '-test_asym' in list(argdict.keys()):
        def cgr(cpg):
            if argdict['-test_asym'] == 'b':
                left_mod = 0.9
                right_mod = 1.2

                left_mod = 0.759
                # left_mod = 0.89
                right_mod = 0.897

                # right_mod = 0.736
                # left_mod = 0.897
            elif argdict['-test_asym'] == 'c':
                left_mod = 1.05
                right_mod = 1.05
            elif argdict['-test_asym'] == 'r':
                left_mod = 1.2
                right_mod = 0.9

                # left_mod = 1.3
                # right_mod = 0.7
            decoupling_functions[argdict['-test_asym_type']](
                cpg, None, left_mod, right_mod)
            # print('\t\t', cpg.x)
            # xbuf = cpg.x
            # xbuf[3] = 0.11
            # cpg.x = xbuf
            # print('\t\t', cpg.x)

            # cpg.r[1][3] = 0.4781
            # cpg.r[3][1] = 0.3499

        plot_best(filename, dirname,
                  an=True, num=False,
                  point=None, cgr=cgr,
                  **pbkwargs)


    # PLOT RESULT
    if '-plot' in list(argdict.keys()) and int(argdict['-plot']):
        plot_best(filename, dirname,
                  **pbkwargs)

    print('Finish Dirname: {}, Pid: {}, Time: {}'.format(
        dirname, os.getpid(), other.time_stamp()))

    pylab.show()
