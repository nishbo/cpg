#!/usr/bin/env python3
import pylab

import cpgpy
import simulator


def main():
    plot = False
    endresult = False
    dt = 0.01
    dt_c = 0.001
    u = 0.3198#0.111
    cpga_strs = ['l', 's', 'sn', 'c']#, 'q', 'penta', 'hexa', 'septa', 'octa', 'nona', 'deca', 'eleven', 'twelve', 'n']
    cpga_descs = ['linear', 'square', 'square num', 'cubic']#, 'quadratic', 'penta', 'hexa', 'septa', 'octa', 'nona', 'deca', 'eleven', 'twelve', 'numerical']

    cpgn = cpgpy.Cpg_Numerical()
    cpgn.dt = dt
    cpgn.u = [u]*4
    cpga_list = []
    for i, n in enumerate(cpga_strs):
        cpga_list.append(cpgpy.Cpg_Analytical(n))
        cpga_list[-1].dt = dt + (i+1)*dt_c
        # if n == 's':
        #     cpga_list[-1].dt = 0.05
        # if n == 'sn':
        #     cpga_list[-1].dt = 0.06
        cpga_list[-1].u = [u]*4

    full_point = [0.20322228, 0.15039054, 1.7361355, 3.51094074, 4.98069952,
                  2.09416532, 0.29899163, 1.50392213, -2.65231136, -0.23240355,
                  4.67104871]
    full_point = [0.23603243, 0.11579432, -0.11878336, 4.97047969, 4.74912488,
                2.53436069, 0.35071898, 1.25063074, -4.99878223, -0.08730851,
                4.6235924]  # second order not enough??!
    full_point = [1.00000000e+00, 3.49785163e-02, 2.86896425e-03,
                  1.76462403e-01, 2.23182924e+00, 2.19419251e+00,
                  1.06652243e+00, -2.01141326e+00, -6.34914213e-01,
                  1.71438434e+00, 3.08689884e+00]
    active = [True, False, False, True]

    ps = simulator.CpgParamSetter('symmetrical')
    def reset(cpgs):
        for cpg in cpgs:
            cpg.reset()
            cpg.active = list(active)
            ps(cpg, full_point)

    if plot:
        reset([cpgn] + cpga_list)

        ta_list = []
        xa_list = []
        for cpg in cpga_list:
            buf = cpg.step_to_change_accumulative()
            ta_list.append(buf[0])
            xa_list.append(buf[1])
        tn, xn = cpgn.step_to_change_accumulative()

        for ta, desc in zip(ta_list, cpga_descs):
            print('Analytical from plot {}: {}'.format(desc, ta[-1]))
        print('Numerical from plot:', tn[-1])
        print()

        pylab.figure()
        pylab.subplots_adjust(hspace=0.1)
        for i in range(4):
            if i == 0:
                ax0 = pylab.subplot(4, 1, 1)
            else:
                pylab.subplot(4, 1, i+1, sharex=ax0, sharey=ax0)

            for ta, xa, n in zip(ta_list, xa_list, cpga_strs):
                pylab.plot(ta, [j[i] for j in xa], 'o-',
                           label='a{}'.format(n), alpha=0.5)
            pylab.plot(tn, [j[i] for j in xn], 'o-', label='n', alpha=0.5)
            pylab.legend()
            pylab.xlabel(str(i+1))
        pylab.ylim([0, 1])

    if endresult:
        reset([cpgn] + cpga_list)
        for cpg, desc in zip(cpga_list, cpga_descs):
            print('Analytical {}: {}'.format(
                desc, cpg.step_to_change()))
        print('Numerical:', cpgn.step_to_change())
        print()

    reset([cpgn] + cpga_list)

    cpga_list[-1].update_params()
    print(cpga_list[-1]._A)
    print(cpga_list[-1]._B)
    print(cpga_list[-1]._x0)
    print(cpga_list[-1]._med(*cpga_list[-1]._D2())[-1])

    for cpg, desc in zip(cpga_list, cpga_descs):
        print('Analytical biased {}: {} {}'.format(
            desc, cpg.get_time_of_change_1(), cpg.get_time_of_change_2()))
    print('Numerical:', cpgn.step_to_change())


if __name__ == '__main__':
    main()

    pylab.show()
