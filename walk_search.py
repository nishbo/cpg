import numpy
from simsimpy import other


def generate_all_directions(length):
    if length < 1:
        return [[]]
    else:
        a = generate_all_directions(length - 1)
        answ = []
        for i in a:
            answ.append(list(i) + [-1])
            answ.append(list(i) + [0])
            answ.append(list(i) + [1])
        return answ


def generate_nondiagonal_directions(length):
    answ = []
    for i in xrange(length):
        answ.append([0]*length)
        answ.append([0]*length)
        answ.append([0]*length)

        answ[-1][i] = -1
        answ[-2][i] = 0
        answ[-3][i] = 1
    return answ


def test_nearby_points(target, point, dx, scale=None, diagonal=False):
    """dx is a scalar"""
    if scale is None:
        scale = [1]*len(point)
    scale = numpy.array(scale)
    point = numpy.array(point)
    target_loc = lambda direction: target(point + direction*scale*dx)

    res = {}
    if diagonal:
        directions = generate_all_directions(len(point))
    else:
        directions = generate_nondiagonal_directions(len(point))

    for direction in directions:
        res[str(direction)] = target_loc(direction)

    return [directions, res]


def walk_recurrent(target, x0, scale, directions, bounds):
    res = []
    for direction in directions:
        x = x0 + direction*scale
        if bounds(x_new=x):
            res.append(target(x))
        else:
            res.append(numpy.inf)

    if 1. - min(res) / target(x0) < 1e-6:
        return x0
    else:
        return walk_recurrent(
            target, x0 + directions[res.index(min(res))]*scale,
            scale, directions, bounds)


def graduate_walk(target, x0, dx=1e-2, scale=None, diagonal=False,
                  bounds=None):
    if scale is None:
        scale = [1]*len(x0)
    x0 = numpy.array(x0)

    if bounds is None:
        bounds = lambda **x: True
    else:
        bounds = other.Bounds(bounds)

    if diagonal:
        directions = generate_all_directions(len(x0))
    else:
        directions = generate_nondiagonal_directions(len(x0))

    for ddx in list(other.dlogrange(0.1, 0.1, stop=dx)) + [dx]:
        scale_loc = numpy.array(scale) * ddx
        x0 = walk_recurrent(target, x0, scale_loc, directions, bounds)

    return x0


def walk(target, x0, dx=1e-2, scale=None, diagonal=False,
         bounds=None):
    """dx is a scalar"""
    if scale is None:
        scale = [1]*len(x0)
    scale = numpy.array(scale) * dx
    x0 = numpy.array(x0)

    if bounds is None:
        bounds = lambda **x: True
    else:
        bounds = other.Bounds(bounds)

    if diagonal:
        directions = generate_all_directions(len(x0))
    else:
        directions = generate_nondiagonal_directions(len(x0))

    return walk_recurrent(target, x0, scale, directions, bounds)
