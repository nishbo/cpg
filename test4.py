import simulator
import cost_function
from simsimpy import other
import numpy
import pylab

simulator.show('analytical', 'symmetrical',
               simulator.CpgParamSetter('symmetrical').x_start(),
               numpy.linspace(
                    cost_function.HALBERTSMA_MIN_SPEED,
                    cost_function.HALBERTSMA_MAX_SPEED,
                    1),
               plot_ruler=False,
               plot_periods=False,
               plot_speedvsinput=False,
               dirname='plots2')

# simulator.show('numerical', 'symmetrical',
#                simulator.CpgParamSetter('symmetrical').x_start(),
#                numpy.linspace(
#                     cost_function.HALBERTSMA_MIN_SPEED,
#                     cost_function.HALBERTSMA_MAX_SPEED,
#                     10),
#                plot_ruler=True,
#                plot_periods=True,
#                plot_speedvsinput=True,
#                dirname='plots2')

pylab.show()
