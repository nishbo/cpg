#!/bin/bash

#PBS -N numvsan
#PBS -q comm_mmem_week
#PBS -l nodes=1:ppn=4

cd locomotion
python main.py
