#!/usr/bin/env python3
from os.path import join
from random import randint


FIGARGS = {
    'figsize': (16., 9.),
    'dpi': 100
}

FIGARGS = {
    'figsize': (4.5, 8.),
    'dpi': 100
}

pps = 16
USE_FULL_POINT = True

US_N = 5

DIRNAME = 'an_comparison_fp' if USE_FULL_POINT else 'an_comparison'
FIGURE_DIR = 'plots2'
n2fn = lambda n: join(DIRNAME, n + '.pcl')

def n2pn(n, biased, ref_to, ext='png'):
    s = 'full_point' if USE_FULL_POINT else 'only_x'
    s += '_biased' if biased else '_unbiased'
    s += '_' + ref_to
    s += '_' + n
    s += '.' + ext
    return join(FIGURE_DIR, s)

coin = lambda : randint(0, 1)
states_avail = [[0, 1], [2, 3]]
# states_avail = [[0], [2]]


def forward_1d_walk(target, x, dx, maxiter=1000):
    cp = target(x)
    np = target(x+dx)
    i = 0
    while cp * np > 0 and i < maxiter:
        x += dx
        i += 1
        cp = np
        np = target(x+dx)
    if i == maxiter:
        print('failed')
        return None
    x += dx / 2
    # if i < 10:
    #     # print 'stop before 10 iters', x, target(x)
    #     # print 'started from', target(0.)
    #     return -1.5
    return x


def predict_finish(time_passed, dt_completed, max_dt):
    return time_passed * (dt_completed / max_dt - 1)
