from scipy import optimize
import nlopt
import walk_search


def push_into_bounds(a, bounds):
    a = list(a)
    for i in xrange(len(a)):
        if a[i] < bounds[i][0]:
            a[i] = bounds[i][0]
        if a[i] > bounds[i][1]:
            a[i] = bounds[i][1]
    return a


def opt_test(x, *args, **kwargs):
    return optimize.rosen(x)


WALK_XTOL_PRECISION = 1e-2


def walk(*args, **kwargs):
    answ = optimize.OptimizeResult()
    answ.x = walk_search.walk(*args, **kwargs)
    answ.success = True
    answ.status = 1
    answ.message = 'Evaluated {} times'.format(
        len(args[0].storage.keys()))
    answ.fun = args[0](answ.x)
    return answ


def graduate_walk(*args, **kwargs):
    answ = optimize.OptimizeResult()
    answ.x = walk_search.graduate_walk(*args, **kwargs)
    answ.success = True
    answ.status = 1
    answ.message = 'Evaluated {} times'.format(
        len(args[0].storage.keys()))
    answ.fun = args[0](answ.x)
    return answ


def walk_diag(*args, **kwargs):
    return walk(
        dx=WALK_XTOL_PRECISION, diagonal=True, bounds=kwargs['bounds'],
        *args)


def walk_ndiag(*args, **kwargs):
    return walk(
        dx=WALK_XTOL_PRECISION, diagonal=False, bounds=kwargs['bounds'],
        *args)


def grad_walk_ndiag(*args, **kwargs):
    return graduate_walk(
        dx=WALK_XTOL_PRECISION, diagonal=False, bounds=kwargs['bounds'],
        *args)


def tnc(*args, **kwargs):
    return optimize.minimize(
        method="TNC",
        bounds=kwargs['bounds'],
        options={
            'accuracy': 1e-2,
            'eps': 1e-2,  # 1e-08
            'scale': kwargs['scale']
        }, *args)


def combined_tnc_grad(*args, **kwargs):
    r1 = tnc(*args, **kwargs)
    args = list(args)
    args[1] = r1['x']
    if kwargs['verbose']:
        print 'Going into walk search.'
    r1 = walk_ndiag(*args, **kwargs)
    return r1


def tnc_twice(*args, **kwargs):
    r1 = tnc(*args, **kwargs)
    args = list(args)
    args[1] = r1['x']
    r1 = tnc(*args, **kwargs)
    return r1


NLOPT_FTOL_PRECISION = 1e-6
NLOPT_RECURRENT_PRECISION_STOP = 1e-2


def nlopt_cobyla_recurrent(*args, **kwargs):
    answ = optimize.OptimizeResult()
    bounds = kwargs['bounds']

    opt = nlopt.opt(nlopt.LN_COBYLA, len(args[1]))
    opt.set_lower_bounds([i[0] for i in bounds])
    opt.set_upper_bounds([i[1] for i in bounds])
    opt.set_ftol_rel(NLOPT_FTOL_PRECISION)
    opt.set_min_objective(args[0])
    # opt.set_min_objective(opt_test)

    x0 = push_into_bounds(args[1], bounds)
    f0 = args[0](x0)

    try:
        x1 = opt.optimize(x0)
    except nlopt.RoundoffLimited:
        answ.x = x0
        answ.fun = f0
        answ.success = False
        answ.message = 'nlopt.RoundoffLimited'
        return answ

    f1 = args[0](x1)
    j = 1
    while abs(1. - f1 / f0) > NLOPT_RECURRENT_PRECISION_STOP:
        print '\tCOBYLA step {}: {}'.format(j, f1)
        j += 1
        x0 = push_into_bounds(x1, bounds)
        f0 = args[0](x0)

        try:
            x1 = opt.optimize(x0)
            f1 = args[0](x1)
        except nlopt.RoundoffLimited:
            x1 = x0
            f1 = f0
            break

    answ.x = x1
    answ.fun = f1

    answ.success = True if opt.last_optimize_result() in [3, 4] else False
    answ.status = j
    answ.message = 'Evaluated {} times in {} searches. Message #{}'.format(
        len(args[0].storage.keys()), j, opt.last_optimize_result())
    return answ


def nlopt_cobyla(*args, **kwargs):
    answ = optimize.OptimizeResult()
    bounds = kwargs['bounds']

    opt = nlopt.opt(nlopt.LN_COBYLA, len(args[1]))
    opt.set_lower_bounds([i[0] for i in bounds])
    opt.set_upper_bounds([i[1] for i in bounds])
    opt.set_ftol_rel(NLOPT_FTOL_PRECISION)
    opt.set_min_objective(args[0])
    # opt.set_min_objective(opt_test)

    x0 = push_into_bounds(args[1], bounds)

    try:
        x1 = opt.optimize(x0)
    except nlopt.RoundoffLimited:
        answ.x = x0
        answ.fun = f0
        answ.success = False
        answ.message = 'nlopt.RoundoffLimited'
        return answ

    answ.success = True if opt.last_optimize_result() in [3, 4] else False
    answ.fun = args[0](x1)
    answ.message = 'Evaluated {} times. Message #{}'.format(
        len(args[0].storage.keys()), opt.last_optimize_result())

    answ.x = x1
    answ.status = 1
    return answ


def combined_cobyla_walk(*args, **kwargs):
    r1 = nlopt_cobyla(*args, **kwargs)
    args = list(args)
    args[1] = r1['x']
    if 'verbose' in kwargs.keys() and kwargs['verbose']:
        print '\tFrom COBYLA to walk search.'
    r1 = walk_ndiag(*args, **kwargs)
    return r1


def combined_cobylarec_walk(*args, **kwargs):
    r1 = nlopt_cobyla_recurrent(*args, **kwargs)
    args = list(args)
    args[1] = r1['x']
    # if 'verbose' in kwargs.keys() and kwargs['verbose']:
    print '\tFrom COBYLA to walk search.'
    r1 = grad_walk_ndiag(*args, **kwargs)
    print 'Search ended with:'
    print r1['x']
    print r1['fun']
    return r1
