# redo of test 2
import math
import numpy.linalg

import simulator

hi = 1

A = [[-0.5041,  2.0618],
     [-2.1359,  1.164 ]]
B = [2.4517382, 0.3706118]
x0 = [0.3185, 0.4352]


A_inv = numpy.linalg.inv(A)

d1 = A_inv[0][0]*B[0] + x0[0],
d2 = A_inv[0][0]*B[1] + x0[1],
d3 = A_inv[0][1]*B[0],
d4 = A_inv[0][1]*B[1],
d5 = hi + A_inv[0][0]*B[0] + A_inv[0][1]*B[1]

s = (A[0][0] + A[1][1]) / 2
st = s - A[1][1]
q = math.sqrt(st**2 + A[0][1]*A[1][0])

z1 = d1 + d4
z2 = st*d1 + A[0][1]*d2 + A[1][0]*d3 - st*d4
z3 = d5

a = z3*s**2 - z1*q**2
b = z3*s + z2
c = z3 - z1
Dt = math.sqrt(b**2 - 2*a*c)
t = (b - Dt) / a
