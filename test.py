#!/usr/bin/env python3
import warnings
import os
import sys
import math
import random
import time
import meta
import pickle
from multiprocessing import Pool
from functools import partial

import numpy
import pylab
import matplotlib.colors as colors

from simsimpy import other

import cpgpy
import meta
import simulator
import cost_function


def generate_setup(iterations, us, near_optimal=False):
    param_setter = simulator.CpgParamSetter('symmetrical')

    full_points = []
    actives = []
    for i in range(iterations):
        if near_optimal:
            actives.append([True, False, False, True])
            full_points.append(param_setter.near_optimal_point(sd=0.01))
        else:
            ans = [random.choice(meta.states_avail[0]),
                   random.choice(meta.states_avail[1])]
            actives.append([False]*4)
            actives[-1][ans[0]] = actives[-1][ans[1]] = True

            full_points.append(param_setter.random_point())

    with open(meta.n2fn('setup'), 'wb') as f:
        pickle.dump([us, actives, full_points], f)


def test_dt_on_random_points(us, actives, full_points, max_dt, dt):
    cpgn = cpgpy.Cpg_Numerical()
    cpgn.dt = dt
    ps = simulator.CpgParamSetter('symmetrical')
    ps(cpgn, ps.x_start())

    nvall = []
    nwhich = []
    iterations = len(actives)
    print('dt', dt, 'started')
    ntimel = time.clock()
    for active, full_point in zip(actives, full_points):
        nvalll = []
        nwhichl = []
        for u in us:
            cpgn.active = list(active)
            if meta.USE_FULL_POINT:
                ps(cpgn, full_point)
            else:
                ps.set_x(cpgn, full_point[:2])

            cpgn.change = [False]*2
            cpgn.u = [u]*4
            nvalll.append(cpgn.step_to_change())
            if nvalll[-1] > 0:
                nwhichl.append([ai and (ai != bi) for ai, bi in zip(
                        active, cpgn.active)].index(True))
                # print(active, cpgn.active, nwhichl[-1])
            else:
                nwhichl.append(None)
        nvall.append(nvalll)
        nwhich.append(nwhichl)
    full_time = time.clock() - ntimel
    ntimel = full_time / (iterations*len(us))
    print('dt', dt, 'finished in', full_time, 'seconds,'
          ' predicted end of simulation in', meta.predict_finish(
              full_time, dt, max_dt))
    return [ntimel, nvall, nwhich]


def test_numerical_on_random_points(dts):
    with open(meta.n2fn('setup'), 'rb') as f:
        us, actives, full_points = pickle.load(f)

    func = partial(test_dt_on_random_points, us, actives, full_points, min(dts))
    p = Pool(meta.pps)
    buf = p.map(func, dts)
    ntime = [i[0] for i in buf]
    nval = [i[1] for i in buf]
    nwhich = [i[2] for i in buf]
    with open(meta.n2fn('numerical'), 'wb') as f:
        pickle.dump([dts, nval, ntime, nwhich], f)

    print('Numerical done.')


def test_analytical_on_random_points(suffix):
    print('Testing solution', suffix)
    with open(meta.n2fn('setup'), 'rb') as f:
        us, actives, full_points = pickle.load(f)
    iterations = len(actives)

    cpga = cpgpy.Cpg_Analytical(solution_type=suffix)
    cpga.dt = 1e-5
    ps = simulator.CpgParamSetter('symmetrical')
    ps(cpga, ps.x_start())

    aval = []
    awhich = []
    atime = time.clock()
    for active, full_point in zip(actives, full_points):
        avall = []
        awhichl = []
        for u in us:
            cpga.reset()
            cpga.active = list(active)
            cpga.u = [u]*4
            if meta.USE_FULL_POINT:
                ps(cpga, full_point)
            else:
                ps.set_x(cpga, full_point[:2])

            cpga.change = [False]*2
            # try:
            avall.append(cpga.step_to_change())
            # except:
            #     print(full_point)
            #     print(active)
            #     sys.exit()
            if avall[-1] > 0:
                awhichl.append(
                    [ai and (ai != bi)
                     for ai, bi in zip(active, cpga.active)].index(True))
            else:
                awhichl.append(-1)
        aval.append(avall)
        awhich.append(awhichl)
    atime = (time.clock() - atime) / (iterations*len(us))

    with open(meta.n2fn('analytical_{}'.format(suffix)), 'wb') as f:
        pickle.dump([aval, atime, awhich], f)

    print('Analytical done.')


def test_biased_analytical_on_random_points(suffix):
    print('Testing biased solution', suffix)
    with open(meta.n2fn('setup'), 'rb') as f:
        us, actives, full_points = pickle.load(f)
    iterations = len(actives)

    with open(meta.n2fn('numerical'), 'rb') as f:
        dts, nvals, ntimes, nwhichs = pickle.load(f)
    print('Numerical bias dt =', dts[-1])
    nwhich = nwhichs[-1]

    cpga = cpgpy.Cpg_Analytical(solution_type=suffix)
    cpga.dt = 1e-5
    ps = simulator.CpgParamSetter('symmetrical')
    ps(cpga, ps.x_start())

    aval = []
    awhich = []
    atime = time.clock()
    for i, (active, full_point) in enumerate(zip(actives, full_points)):
        avall = []
        awhichl = []
        for j, u in enumerate(us):
            cpga.reset()
            cpga.active = list(active)
            cpga.u = [u]*4
            if meta.USE_FULL_POINT:
                ps(cpga, full_point)
            else:
                ps.set_x(cpga, full_point[:2])

            cpga.change = [False]*2
            if nwhich[i][j] is None:
                avall.append(cpga.step_to_change())
            elif nwhich[i][j] < 2:
                avall.append(cpga.get_time_of_change_1())
                # print('1: ', avall[-1])
            else:
                avall.append(cpga.get_time_of_change_2())
                # print('2: ', avall[-1])

            if avall[-1] is not None:
                awhichl.append(nwhich[i][j])
                # print(active, cpga.active, awhichl[-1])
            else:
                avall[-1] = -1
                awhichl.append(None)
        aval.append(avall)
        awhich.append(awhichl)
    atime = (time.clock() - atime) / (iterations*len(us))

    with open(meta.n2fn('biased_analytical_{}'.format(suffix)), 'wb') as f:
        pickle.dump([aval, atime, awhich], f)

    print('Biased analytical done.')


def print_compare_a(suffixes, reference, compare_with_bias,
                    reference_analytical_index=-1,
                    plot_precision_speed=False,
                    plot_error_distribution=False,
                    plot_compare_analyticals=False,
                    closelook_around_the_points=(),
                    closelook_radius=0.0006,
                    use_numerical_for_error_ditribution=True):
    # Exctract data
    with open(meta.n2fn('setup'), 'rb') as f:
        us, actives, full_points = pickle.load(f)
    with open(meta.n2fn('numerical'), 'rb') as f:
        dts, nvals, ntimes, nwhichs = pickle.load(f)

    pfname = 'biased_analytical_{}' if compare_with_bias else 'analytical_{}'
    avals = []
    atimes = []
    awhichs = []
    for suffix in suffixes:
        with open(meta.n2fn(pfname.format(suffix)), 'rb') as f:
            aval, atime, awhich = pickle.load(f)
            avals.append(aval)
            atimes.append(atime)
            awhichs.append(awhich)

    # Find which ones were solved properly
    nsolved = []
    for nval in nvals:
        nsolved.append([])
        for j, nva in enumerate(nval):
            nsolved[-1] += [[j, i] for i, nv in enumerate(nva) if nv > 0]
    asolved = []
    for aval in avals:
        asolved.append([])
        for j, ava in enumerate(aval):
            asolved[-1] += [[j, i] for i, av in enumerate(ava) if av > 0]

    # intp = 2
    # up = 0
    # cmp1 = 2
    # cmp2 = 1
    # cmpref = -1
    # # endp = 500000
    # # print([e[0] for e in nsolved[-1][:100] if e[0] < endp and e[1] == 0])
    # # print([e[0] for e in asolved[0][:100] if e[0] < endp and e[1] == 0])
    # # print([e[0] for e in asolved[1][:100] if e[0] < endp and e[1] == 0])
    # # print([e[0] for e in asolved[2][:100] if e[0] < endp and e[1] == 0])
    # # print()
    # # print([e[0] for e in nvals[-1][:endp]])
    # # print([e[0] for e in avals[0][:endp]])
    # # print([e[0] for e in avals[1][:endp]])
    # # print([e[0] for e in avals[2][:endp]])
    # # print()
    # # for e in [e for e in asolved[-1] if e[1] == 0]:
    # for n in nsolved[-1]:
    #     if n not in asolved[-1]:
    #         print('found', n)
    #         intp = n[0]
    #         up = n[1]
    #         break

    # # for e in asolved[cmpref]:
    # #     # if e not in asolved[2]:
    # #     if (abs(avals[cmp1][e[0]][e[1]] - avals[cmpref][e[0]][e[1]]) > 0.1 and
    # #             abs(avals[cmp2][e[0]][e[1]] - avals[cmpref][e[0]][e[1]]) < 0.1 and
    # #             avals[cmp1][e[0]][e[1]] > 0 and avals[cmp2][e[0]][e[1]] > 0):
    # #         print('found', e)
    # #         intp = e[0]
    # #         up = e[1]
    # #         break
    # # print([e[0] for e in asolved[2][:endp]])
    # print(actives[intp])
    # print(full_points[intp])
    # print(us[up])
    # sys.exit()

    print('Amounts of solved points:')
    print([len(nso) for nso in nsolved])
    print([len(aso) for aso in asolved])

    best_dt_id = dts.index(min(dts))
    if reference == 'num':
        print('These should be equal. The best precision of numerical solution'
              ' will be used as a reference.')
        reference_vals = nvals[best_dt_id]
        reference_solved = nsolved[best_dt_id]
    elif reference == 'an':
        print('These should be equal. The {} analytical solution'
              ' will be used as a reference.'.format(
                  suffixes[reference_analytical_index]))
        reference_vals = avals[reference_analytical_index]
        reference_solved = asolved[reference_analytical_index]
    flat_reference = [reference_vals[i[0]][i[1]] for i in reference_solved]
    maxval_reference = max(flat_reference)
    flat_solved = set(i[0] for i in reference_solved)
    solved_full_points = [full_points[i] for i in flat_solved]
    solved_actives = [actives[i] for i in flat_solved]

    # diff_distrib_f = lambda x: [
    #     x[i[0]][i[1]] - reference_vals[i[0]][i[1]]
    #     if x[i[0]][i[1]] > 0 else reference_vals[i[0]][i[1]]
    #     for i in reference_solved]  # Maxes unknown max
    diff_distrib_f = lambda x: [
        x[i[0]][i[1]] - reference_vals[i[0]][i[1]]
        for i in reference_solved
        if x[i[0]][i[1]] > 0]  # Skips unknown
    # ndiff_distrib_f = lambda x: [
    #     (1 - x[i[0]][i[1]] / reference_vals[i[0]][i[1]]
    #      if reference_vals[i[0]][i[1]] > 1e-4 else 0)
    #     if x[i[0]][i[1]] > 0 else 1
    #     for i in reference_solved]  # Maxes unknown max
    ndiff_distrib_f = lambda x: [
        1 - x[i[0]][i[1]] / reference_vals[i[0]][i[1]]
        if reference_vals[i[0]][i[1]] > 1e-4 else 0
        for i in reference_solved
        if x[i[0]][i[1]] > 0]  # Skips unknown
    rms_f = lambda x: math.sqrt(sum(di**2 for di in x) / len(x))
    rms2max_f = lambda x: rms_f(x) / maxval_reference

    if plot_precision_speed:
        dt_xrange = [max(dts), min(dts)]
        fig = pylab.figure(figsize=(6, 12), dpi=100)
        pylab.suptitle('Comparison of anaytical and numerical solutions')
        fig.subplots_adjust(hspace=0.01)

        ax = pylab.subplot(4, 1, 1)
        pylab.semilogx(dts, [rms_f(diff_distrib_f(nval)) for nval in nvals],
                       label='numerical integration')
        for suffix, aval in zip(suffixes, avals):
            pylab.semilogx(dt_xrange, [rms_f(diff_distrib_f(aval))]*2,
                           label='analytical {}'.format(suffix))
        pylab.xlim(dt_xrange)
        pylab.legend()
        pylab.ylabel('RMS, s')
        ax.xaxis.set_ticklabels([])

        ax = pylab.subplot(4, 1, 2)
        pylab.semilogx(dts, [rms_f(ndiff_distrib_f(nval)) for nval in nvals],
                       label='numerical integration')
        for suffix, aval in zip(suffixes, avals):
            pylab.semilogx(dt_xrange, [rms_f(ndiff_distrib_f(aval))]*2,
                           label='analytical {}'.format(suffix))
        pylab.xlim(dt_xrange)
        pylab.legend()
        pylab.ylabel('Normalized difference RMS, au')
        ax.xaxis.set_ticklabels([])

        ax = pylab.subplot(4, 1, 3)
        pylab.semilogx(
            dts,
            [rms2max_f(diff_distrib_f(nval)) for nval in nvals],
            label='numerical integration')
        for suffix, aval in zip(suffixes, avals):
            pylab.semilogx(
                dt_xrange, [rms2max_f(diff_distrib_f(aval))]*2,
                label='analytical {}'.format(suffix))
        pylab.xlim(dt_xrange)
        pylab.legend()
        pylab.ylabel('RMS normalized to max value, au')
        ax.xaxis.set_ticklabels([])

        ax = pylab.subplot(4, 1, 4)
        pylab.loglog(dts, ntimes,
                       label='numerical integration')
        for suffix, atime in zip(suffixes, atimes):
            pylab.loglog(dt_xrange, [atime]*2,
                         label='analytical {}'.format(suffix))
        pylab.xlim(dt_xrange)
        pylab.ylim([min(atimes)*0.9, 2*max(ntimes)])
        pylab.legend()
        pylab.ylabel('Time of step evaluation, s')
        pylab.xlabel('Numerical integration step, s')

        pylab.savefig(meta.n2pn('precision_speed', compare_with_bias,
                                reference, 'png'))
        pylab.savefig(
            meta.n2pn('precision_speed', compare_with_bias, reference, 'pdf'),
            format='pdf')

    if plot_compare_analyticals:
        xlims = [-0.5, len(suffixes)-0.5]
        fig = pylab.figure(figsize=(8, 12), dpi=100)
        pylab.suptitle('Comparison of anaytical solutions')
        fig.subplots_adjust(hspace=0.01)

        ax = pylab.subplot(4, 1, 1)
        vals = []
        for i, (suffix, aval) in enumerate(zip(suffixes, avals)):
            vals.append(rms_f(diff_distrib_f(aval)))
            pylab.plot(i, vals[-1], 'o')
        pylab.plot(range(len(vals)), vals, 'b')
        pylab.xlim(xlims)
        pylab.ylabel('RMS, s')
        ax.xaxis.set_ticklabels([])

        ax = pylab.subplot(4, 1, 2)
        vals = []
        for i, (suffix, aval) in enumerate(zip(suffixes, avals)):
            vals.append(rms_f(ndiff_distrib_f(aval)))
            pylab.plot(i, vals[-1], 'o')
        pylab.plot(range(len(vals)), vals, 'b')
        pylab.xlim(xlims)
        pylab.ylabel('Normalized difference RMS, au')
        ax.xaxis.set_ticklabels([])

        ax = pylab.subplot(4, 1, 3)
        vals = []
        for i, (suffix, aval) in enumerate(zip(suffixes, avals)):
            vals.append(rms2max_f(diff_distrib_f(aval)))
            pylab.plot(i, vals[-1], 'o')
        pylab.plot(range(len(vals)), vals, 'b')
        pylab.xlim(xlims)
        pylab.ylabel('RMS normalized to max value, au')
        ax.xaxis.set_ticklabels([])

        ax = pylab.subplot(4, 1, 4)
        vals = []
        for i, (suffix, atime) in enumerate(zip(suffixes, atimes)):
            vals.append(atime)
            pylab.plot(i, vals[-1], 'o')
        pylab.plot(range(len(vals)), vals, 'b')
        pylab.xlim(xlims)
        pylab.ylabel('Time of step evaluation, s')
        pylab.xlabel('Type of analytical solution')
        ax.xaxis.set_ticks(range(len(suffixes)))
        ax.xaxis.set_ticklabels(suffixes)

        pylab.savefig(meta.n2pn('analyticals_comparison', compare_with_bias,
                                reference, 'png'))
        pylab.savefig(
            meta.n2pn('analyticals_comparison', compare_with_bias, reference, 'pdf'),
            format='pdf')

    if plot_error_distribution:
        fig = pylab.figure(figsize=(10, 10), dpi=100)
        pylab.suptitle('Error distributions')
        # fig.subplots_adjust(hspace=0.01)

        pylab.subplot(2, 1, 1)
        print('RMS, s')
        if use_numerical_for_error_ditribution:
            for dt, nval in zip(dts, nvals):
                a = diff_distrib_f(nval)

                amean = numpy.mean(a)
                astd = numpy.std(a)
                print('\tdt: {}, mean diff: {}, std: {}'.format(
                    dt, amean, astd))

                n, bins, patches = pylab.hist(
                    a, 1000, histtype='step',
                    label='num dt={}'.format(dt))
        for suffix, aval in zip(suffixes, avals):
            a = diff_distrib_f(aval)
            alen1 = len(a)
            # a = [ai for ai in a if 0.013 > ai > -0.1]

            print('\tan: {}, {:.2%} were thrown out'.format(
                suffix, (alen1-len(a))/alen1))

            amean = numpy.mean(a)
            astd = numpy.std(a)
            print('\tan: {}, mean diff: {}, std: {}'.format(
                suffix, amean, astd))

            n, bins, patches = pylab.hist(
                a, 1000, histtype='step',
                label='an {}'.format(suffix))
        pylab.legend()
        pylab.ylabel('Portion of evals')
        pylab.xlabel('RMS, s')

        pylab.subplot(2, 1, 2)
        print('Normalized difference RMS, au')
        maxn = 0
        if use_numerical_for_error_ditribution:
            for dt, nval in zip(dts, nvals):
                a = ndiff_distrib_f(nval)

                amean = numpy.mean(a)
                astd = numpy.std(a)
                print('\tdt: {}, mean diff: {}, std: {}'.format(
                    dt, amean, astd))

                n, bins, patches = pylab.hist(
                    a, 1000, histtype='step',
                    label='num dt={}'.format(dt))
                maxn = max(maxn, max(n))
        for suffix, aval in zip(suffixes, avals):
            a = ndiff_distrib_f(aval)
            alen1 = len(a)

            # a = [ai for ai in a if ai > -.5]
            print('\tan: {}, {} {:.2%} were thrown out'.format(
                suffix, alen1-len(a), (alen1-len(a))/alen1))

            amean = numpy.mean(a)
            astd = numpy.std(a)
            print('\tan: {}, mean diff: {}, std: {}'.format(
                suffix, amean, astd))

            n, bins, patches = pylab.hist(
                a, 1000, histtype='step',
                label='an {}'.format(suffix))
            maxn = max(maxn, max(n))


        if len(closelook_around_the_points) > 0:
            for point in closelook_around_the_points:
                pylab.plot([point-closelook_radius]*2, [0, maxn], 'r')
                pylab.plot([point+closelook_radius]*2, [0, maxn], 'r')
        pylab.legend()
        pylab.ylabel('Portion of evals')
        pylab.xlabel('Normalized difference RMS, au')

        pylab.savefig(meta.n2pn('error_distribution', compare_with_bias,
                                reference, 'png'))
        pylab.savefig(
            meta.n2pn('error_distribution', compare_with_bias, reference, 'pdf'),
            format='pdf')

    if len(closelook_around_the_points) > 0:
        raise DeprecationWarning()
        pylab.figure(figsize=(4*len(closelook_around_the_points), 13), dpi=100)
        reference2 = avals[-1]
        a = ndiff_distrib_f(reference2)
        hl = len(closelook_around_the_points)+1

        acx = ['tftf', 'fttf', 'tfft', 'ftft']
        acxn = [5, 6, 9, 10]
        ac2n = lambda ac: acxn.index(
            sum(int(ai)*2**i for i, ai in enumerate(ac)))
        points_assesed = 0
        for i, point in enumerate(closelook_around_the_points):
            point_vals_idxs = list(
                reference_solved[j] for j, ai in enumerate(a)
                if (point-closelook_radius < ai < point+closelook_radius))
            point_idxs = set(j[0] for j in point_vals_idxs)
            # print(sorted(point_vals_idxs))

            xs = [solved_full_points[j][:2] for j in point_idxs]
            ases = [solved_actives[j] for j in point_idxs]
            asestrans = [ac2n(ac) for ac in ases]
            inputs = [j[1] for j in point_vals_idxs]
            diffs = diff_distrib_f([
                reference2[p[0]][p[1]] - reference_vals[p[0]][p[1]]
                for p in point_vals_idxs])

            points_assesed += len(point_idxs)

            pylab.subplot(4, hl, i+1)
            pylab.plot([x[0] for x in xs], [x[1] for x in xs], '*r')
            pylab.xlim([0, 1])
            pylab.ylim([0, 1])
            pylab.title('point {} N={}'.format(point, len(point_idxs)))

            pylab.subplot(4, hl, i+1+hl)
            pylab.hist(asestrans, bins=[-0.5, 0.5, 1.5, 2.5, 3.5])
            pylab.xlim([-1, 4])
            pylab.xticks(range(4), acx)

            pylab.subplot(4, hl, i+1+2*hl)
            bins = [-0.5] + [j+0.5 for j in range(len(us))]
            pylab.hist(inputs, bins=bins)
            pylab.xlim([-1, len(us)+1])
            pylab.xlabel('Input level')

            pylab.subplot(4, hl, i+1+3*hl)
            pylab.hist(diffs, bins=100)
            # pylab.xlim([-1, len(us)+1])
            pylab.xlabel('RMS, s')
        # print([reference_solved[j][0] for j, ai in enumerate(a)])

        pylab.subplot(4, hl, hl)
        pylab.plot([x[0] for x in full_points],
                   [x[1] for x in full_points],
                   '.k')
        pylab.plot([x[0] for x in solved_full_points],
                   [x[1] for x in solved_full_points],
                   '.r')
        pylab.xlim([0, 1])
        pylab.ylim([0, 1])
        pylab.title('all N={}'.format(len(solved_full_points)))

        pylab.subplot(4, hl, 2*hl)
        pylab.hist([ac2n(ac) for ac in actives], color='k',
                   bins=[-0.5, 0.5, 1.5, 2.5, 3.5])
        pylab.hist([ac2n(ac) for ac in solved_actives], color='r',
                   bins=[-0.2, 0.3, 0.7, 1.3, 1.7, 2.3, 2.7, 3.3])
        pylab.xlim([-1, 4])
        pylab.xticks(range(4), acx)

        print('{:.4%} of points are concentrated around the chosen values. '
              'Remember that there are a few inputs per point'.format(
                points_assesed/len(solved_full_points)))

        pylab.savefig(meta.n2pn('closelook_around_points'))
        pylab.savefig(meta.n2pn('closelook_around_points', compare_with_bias,
                                reference, 'png'))
        pylab.savefig(
            meta.n2pn('closelook_around_points', compare_with_bias, reference, 'pdf'),
            format='pdf')


def plot_point_distribution():
    with open(meta.n2fn('setup'), 'rb') as f:
        us, actives, full_points = pickle.load(f)

    pylab.figure()
    pylab.suptitle('all N={}'.format(len(actives)))

    pylab.subplot(2, 1, 1)
    pylab.plot([x[0] for x in full_points], [x[1] for x in full_points], '*r')
    pylab.xlim([0, 1])
    pylab.ylim([0, 1])

    acx = ['tftf', 'fttf', 'tfft', 'ftft']
    ac2n = lambda ac: [5, 6, 9, 10].index(
        sum(int(ai)*2**i for i, ai in enumerate(ac)))
    asestrans = [ac2n(ac) for ac in actives]

    pylab.subplot(2, 1, 2)
    pylab.hist(asestrans, bins=[-0.5, 0.5, 1.5, 2.5, 3.5])
    pylab.xlim([-1, 4])
    pylab.xticks(range(4), acx)


def plot_unsolved_point_distribution(unsolved=True, biased=True):
    if unsolved:
        condition = lambda av: None in av or any(i < 0 for i in av)
    else:
        condition = lambda av: None not in av and all(i > 0 for i in av)

    with open(meta.n2fn('setup'), 'rb') as f:
        us, actives, full_points = pickle.load(f)

    pfname = 'biased_analytical_n' if biased else 'analytical_n'
    with open(meta.n2fn(pfname), 'rb') as f:
        aval, atime, awhich = pickle.load(f)

    uns_idxs = []
    for i, av in enumerate(aval):
        if condition(av):
            uns_idxs.append(i)
    # print(uns_idxs)
    # print(aval)

    flat_awhich = [i[0] for i in awhich]
    color_code = ['r', 'b', 'm', 'k']

    pylab.figure()
    pylab.suptitle('all {} N={}'.format(
        'unsolved' if unsolved else 'solved', len(uns_idxs)))

    ac_s = ['tftf', 'fttf', 'tfft', 'ftft']
    ac_l = [[True, False, True, False], [False, True, True, False],
            [True, False, False, True], [False, True, False, True]]

    for i, (s, l) in enumerate(zip(ac_s, ac_l)):
        pylab.subplot(2, 2, i+1)
        pylab.title(s)
        idxs = [j for j in uns_idxs if actives[j] == l]

        pylab.scatter(
            [full_points[j][0] for j in idxs],
            [full_points[j][1] for j in idxs],
            # c=[color_code[flat_awhich[j]] for j in idxs],
            alpha=0.5)

        for i in range(4):
            pylab.plot(-1, -1, color_code[i]+'.', label=str(i+1))

        pylab.xlim([0, 1])
        pylab.ylim([0, 1])

        pylab.legend(loc='best', fancybox=True, framealpha=0.5)


if __name__ == '__main__':
    warnings.filterwarnings('error')
    #### Which solutions to test
    suffixes = ['l', 's', 'c', 'q', 'n']
    # suffixes = ['l', 's', 'sn', 'c', 'q', 'penta', 'hexa', 'septa', 'octa', 'nona', 'deca', 'eleven', 'twelve', 'n']
    # suffixes = ['l', 's', 'c', 'q', 'penta', 'hexa', 'septa', 'octa', 'nona', 'n']
    # suffixes = ['l', 's', 'n']

    #### Generating configuration for simulations
    # generate_setup(
    #     10000, numpy.linspace(
    #         cost_function.HALBERTSMA_MIN_SPEED,
    #         cost_function.HALBERTSMA_MAX_SPEED,
    #         6),
    #     near_optimal=True,
    #     )
    # generate_setup(100, [1])

    #### Numerical eval
    # test_numerical_on_random_points(
    #     dts=list(other.dlogrange(1e-1, 0.3, stop=1e-4)))
    # test_numerical_on_random_points(dts=[1e-2])

    # #### Analytical eval
    # for suf in suffixes:
    #     test_analytical_on_random_points(suf)
    # for suf in suffixes:
    #     test_biased_analytical_on_random_points(suf)
    # test_analytical_on_random_points('n')
    # test_biased_analytical_on_random_points('s')

    #### Print and plot
    print_compare_a(suffixes, 'an', False,
                    plot_precision_speed=True,
                    plot_compare_analyticals=True,
                    # reference_analytical_index=2,
                    # plot_error_distribution=True,
                    # closelook_around_the_points=(
                    #     -0.156, -0.044, 0.016, 0.055, 0.0748, 0.136, 0.196,
                    #     0.256, 0.265, 0.475, 0.685, 0.895),
                    # closelook_around_the_points=(
                    #     -0.156, -0.044),
                    # use_numerical_for_error_ditribution=False,
                    )
    # plot_point_distribution()
    # plot_unsolved_point_distribution(
    #     unsolved=True,
    #     # biased=False
    #     )
    # plot_unsolved_point_distribution(
    #     unsolved=False,
    #     biased=False
    #     )

    pylab.show()
