from numpy import array


class Optimization_Call_Wrapper(object):
    """Stores already calculated values for recalls.

    Use when time needed to calculate a point is big.

    scale is immutable

    """
    def __init__(self, try_point_f, scale=None):
        self.storage = {}
        if scale is None:
            self.try_point_f = try_point_f
        else:
            self._try_point_f_original = try_point_f
            self.try_point_f = lambda point: try_point_f(array(scale) * point)
        self._i = 0
        self._talk_i = 500
        # self.__call__ = self._call

    def verbose():
        doc = "Want some output?"

        def fget(self):
            return self._verbose

        def fset(self, value):
            self._verbose = value
            if value:
                self._verbose_f = self._verbose_len
            else:
                self._verbose_f = self._verbose_none

        return locals()
    verbose = property(**verbose())

    def _verbose_len(self):
        print len(self.storage.keys()), '\r',

    def _verbose_none(self):
        pass

    def _verbose_f(self):
        pass

    def _call(self, *args, **kwargs):
        raise DeprecationWarning
        point = args[0]
        stp = str(point)
        try:
            return self.storage[stp]
        except KeyError:
            self.storage[stp] = self.try_point_f(point)
        return self.storage[stp]

    def best(self):
        m = min(self.storage, key=self.storage.get)
        return m, self.storage[m]

    def __call__(self, *args, **kwargs):
        point = args[0]
        stp = str(point)

        self._verbose_f()

        try:  # See if point was calculated already
            return self.storage[stp]
        except KeyError:
            try:
                self.storage[stp] = self.try_point_f(point)
            except TypeError:  # Used for point=None
                self.storage[stp] = self._try_point_f_original(point)

        if self._i > self._talk_i:
            self._i = 0
            print 'Best result so far:', self.best()
        else:
            self._i += 1

        return self.storage[stp]

    def __len__(self):
        return len(self.storage)

    def __str__(self):
        return 'Function evaluated {} times.'.format(len(self))
