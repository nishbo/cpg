#!/usr/bin/env python3
import sys
import os
import re
from multiprocessing import Process, Array
import random
import copy
from mpl_toolkits.mplot3d import Axes3D
import time

import numpy
import numpy.random
try:
    import pylab
except:
    pylab = None

import scipy
import scipy.stats
import scipy.optimize as optimize
import scipy.linalg as linalg

import cpgpy
from simsimpy import intstep, other
import cost_function
import meta


class CpgParamSetter(object):
    """Sets parameters in different fashions for a CPG"""
    def __init__(self, style):
        self.style = style
        if style == 'symmetrical':
            self._call = self._call_symmetrical
            self.x_start = self._x_start_symmetrical
            self.cpg_point = self._cpg_point_symmetrical
            self.bounds = self._bounds_symmetrical

    def _call(self, cpg, v):
        raise NotImplementedError()

    def __call__(self, cpg, v):
        preset_cpg_params(cpg, 'zeros')
        self._call(cpg, v)

    def x_start(self):
        raise NotImplementedError()

    def bounds(self):
        raise NotImplementedError()

    def cpg_point(self, cpg):
        raise NotImplementedError()

    def set_x(self, cpg, x):
        cpg.x = [x[i//2] if cpg.active[i] else 0. for i in range(4)]

    def get_x(self, cpg):
        return [cpg.x[int(cpg.active[1])], cpg.x[int(cpg.active[3])]]

    def random_point(self):
        return [random.uniform(*i) for i in self.bounds()]

    def near_optimal_point(self, sd=0.05, mean=None):
        if mean is None:
            mean = self.x_start()
        dim = len(mean)
        cov = numpy.zeros((dim, dim))
        for i in range(dim):
            cov[i][i] = abs(mean[i]*sd)
        probe = numpy.random.multivariate_normal(mean, cov)
        bounds = self.bounds()
        for i in range(dim):
            if probe[i] < bounds[i][0]:
                probe[i] = bounds[i][0]
            elif probe[i] > bounds[i][1]:
                probe[i] = bounds[i][1]
        return probe

    def _call_symmetrical(self, cpg, v):
        self.set_x(cpg, v[:2])
        cpg.r[0][2] = cpg.r[2][0] = 0.  # ff walking
        cpg.r[0][3] = cpg.r[2][1] = v[2]  # ef
        cpg.r[1][2] = cpg.r[3][0] = v[3]  # fe
        cpg.r[1][3] = cpg.r[3][1] = v[4]  # ee
        cpg.g_u[0] = cpg.g_u[2] = v[5]
        cpg.g_u[1] = cpg.g_u[3] = v[6]
        cpg.leak[0] = cpg.leak[2] = v[7]
        cpg.leak[1] = cpg.leak[3] = v[8]
        cpg.u0[0] = cpg.u0[2] = v[9]
        cpg.u0[1] = cpg.u0[3] = v[10]

    def _x_start_symmetrical(self):
        # return [0.1, 0.3, -0.0485, -0.0823, 0.0981, 1.2406, 0.9764,
        #         -0.0094, -0.0094, -0.0007, 2.4256]
        # return [0.3185, 0.4352, 2.0618, -2.1359, 0.0634, 1.5562, 2.9938,
        #         -0.5041, 1.164, 2.279, 0.0383]
        # return [0.5]*2 + [0]*3 + [2.5]*2 + [0]*4
        # return [ 0.87907357,  0.87907357,  1.61937408,  0.25395654,  1.16008413,
        #         2.80690317,  2.81666576, -0.21200913, -0.79258418,  0.53743911,
        #         1.27752954]
        # return [0]*11
        # return [0.23603243, 0.11579432, -0.11878336, 4.97047969, 4.74912488,
        #         2.53436069, 0.35071898, 1.25063074, -4.99878223, -0.08730851,
        #         4.6235924]
        return [0.95865394, 0.03475441, 0.01080399, 0.17822837, 2.34933502,
                2.2579919, 0.93687886, -1.91370984, -0.6386934, 1.75021447,
                2.9392276 ]

    def _bounds_symmetrical(self):
        return [( 0, 1), ( 0, 1),           # x
                (-5, 5), (-5, 5), (-5, 5),  # r
                ( 0, 5), ( 0, 5),           # g_u
                (-5, 5), (-5, 5),           # leak
                (-5, 5), (-5, 5)]           # ci

    def _cpg_point_symmetrical(self, cpg):
        if (cpg.r[0][3] != cpg.r[2][1] or
                cpg.r[1][2] != cpg.r[3][0] or
                cpg.r[1][3] != cpg.r[3][1] or
                cpg.g_u[0] != cpg.g_u[2] or
                cpg.g_u[1] != cpg.g_u[3] or
                cpg.leak[0] != cpg.leak[2] or
                cpg.leak[1] != cpg.leak[3] or
                cpg.u0[0] != cpg.u0[2] or
                cpg.u0[1] != cpg.u0[3]):
            raise Warning('Some coupling associated with symmetrical'
                          ' model is wrong.')
        x = self.get_x(cpg)
        v[0] = x[0]
        v[1] = x[1]
        v[2] = cpg.r[0][3] = cpg.r[2][1]
        v[3] = cpg.r[1][2] = cpg.r[3][0]
        v[4] = cpg.r[1][3] = cpg.r[3][1]
        v[5] = cpg.g_u[0] = cpg.g_u[2]
        v[6] = cpg.g_u[1] = cpg.g_u[3]
        v[7] = cpg.leak[0] = cpg.leak[2]
        v[8] = cpg.leak[1] = cpg.leak[3]
        v[9] = cpg.u0[0] = cpg.u0[2]
        v[10] = cpg.u0[1] = cpg.u0[3]


def preset_cpg_params(cpg, preset):
    if preset == 'zeros':
        cpg.r = numpy.zeros((4, 4))
        cpg.g_u = [0.]*4
        cpg.leak = [0.]*4
        cpg.u0 = [0.]*4
    elif preset == 'sergiy':
        cpg.r = numpy.array([[0.,      0.,      0.1339, -0.0485],
                             [0.,      0.,      -0.0823, 0.0981],
                             [0.1339, -0.0823,  0.,      0.],
                             [-0.0485, 0.0981,  0.,      0.]])
        cpg.g_u = [0.6203, 0.4882]*2
        cpg.leak = [-0.0094]*4
        cpg.u0 = [-0.0007, 2.4256]*2
    elif preset == 'sergiy_sym':
        cpg.r = numpy.array([[0.,      0.,      0.1339, -0.0485],
                             [0.,      0.,      -0.0485, 0.0981],
                             [0.1339, -0.0485,  0.,      0.],
                             [-0.0485, 0.0981,  0.,      0.]])
        cpg.g_u = [0.6203, 0.4882]*2
        cpg.leak = [-0.0094]*4
        cpg.u0 = [-0.0007, 2.4256]*2


def get_periods_vs_input(cpg_type, cpg_style, point, us,
                         active=[True, False, False, True]):
    cpg = cpgpy.init_cpg(cpg_type)
    param_setter = CpgParamSetter(cpg_style)

    ts = [0]*len(us)
    periodss = [0]*len(us)
    overlapss = [0]*len(us)
    for i, u in enumerate(us):
        cpg.active = list(active)
        param_setter(cpg, point)
        cpg.u = [u]*4
        ts[i], periodss[i], overlapss[i] = cpg.walk_full_step()

    return (ts, periodss, overlapss)

def show(cpg_type, cpg_style, point, us,
         active=[True, False, False, True],
         cgr=None,
         plot_ruler=False, plot_periods=False, plot_speedvsinput=False,
         dirname=None, run_costfunction=False,
         uncouple_fun=None, uncouple_pcts=None,
         plot_resolution=None, two_steps=False):
    n2fn = lambda name: os.path.join(
        '' if dirname is None else dirname,
        'type {} style {} nus {} {}.png'.format(
            cpg_type, cpg_style, len(us), name))
    cpg = cpgpy.init_cpg(cpg_type)
    if plot_resolution is not None:
        cpg.dt = plot_resolution
    param_setter = CpgParamSetter(cpg_style)

    ts = [0]*len(us)
    periodss = [0]*len(us)
    overlapss = [0]*len(us)
    timelines = [0]*len(us)
    xlines = [0]*len(us)
    for i, u in enumerate(us):
        cpg.flush()
        cpg.active = list(active)
        param_setter(cpg, point)
        if cgr is not None:
            cgr(cpg)
        cpg.u = [u]*4
        if two_steps:
            ts[i], periodss[i], overlapss[i], timelines[i], xlines[i] = (
            cpg.walk_two_full_steps_using_step())
        else:
            ts[i], periodss[i], overlapss[i], timelines[i], xlines[i] = (
                cpg.walk_full_step_using_step())

    if plot_ruler:
        fig = pylab.figure(**meta.FIGARGS)
        fig.subplots_adjust(wspace=0.1, hspace=0.01)
        pylab.suptitle('Oscillator states. green-flexor, blue-extensor')
        for i, u in enumerate(us):
            xmax = max(timeline[-1] for timeline in timelines)

            ax = pylab.subplot(2, len(us), i+1)
            # pylab.plot(timelines[i], [j[0] for j in xlines[i]], 'g')
            pylab.fill_between(timelines[i], 0, [j[0] for j in xlines[i]],
                               facecolor='g')
            # pylab.plot(timelines[i], [j[1] for j in xlines[i]], 'b')
            pylab.fill_between(timelines[i], 0, [j[1] for j in xlines[i]],
                               facecolor='b')
            pylab.xlim([0, xmax])
            pylab.ylim([0, 1])
            pylab.title('u={:.3f}'.format(u))
            if i == 0:
                pylab.ylabel('x1, x2, au')
            else:
                ax.yaxis.set_ticklabels([])
            ax.xaxis.set_ticklabels([])

            ax = pylab.subplot(2, len(us), len(us)+i+1)
            # pylab.plot(timelines[i], [j[2] for j in xlines[i]], 'g')
            pylab.fill_between(timelines[i], 0, [j[2] for j in xlines[i]],
                               facecolor='g')
            # pylab.plot(timelines[i], [j[3] for j in xlines[i]], 'b')
            pylab.fill_between(timelines[i], 0, [j[3] for j in xlines[i]],
                               facecolor='b')
            pylab.xlim([0, xmax])
            pylab.ylim([0, 1])
            if i == 0:
                pylab.ylabel('x3, x4, au')
            else:
                ax.yaxis.set_ticklabels([])
            if i == len(us) // 2:
                pylab.xlabel('Time, s')

            pylab.savefig(n2fn('ruler'))

    if plot_periods:
        pylab.figure(**meta.FIGARGS)
        pylab.suptitle('Periods of stance and swing phases')
        p01 = [p[0]+p[1] for p in periodss]
        p23 = [p[2]+p[3] for p in periodss]
        p01mm = (min(p01), max(p01))
        p23mm = (min(p23), max(p23))
        xlims = (0,  # 0.9*min(p01mm[0], p23[0]),
                 1.1*max(p01mm[1], p23mm[1]))
        ylims = (0,  # 0.9*min(min(p) for p in periodss),
                 1.1*max(max(p) for p in periodss))

        pylab.subplot(1, 2, 1)
        pylab.title('left')
        pylab.plot(p01, [p[0] for p in periodss], 'g*',
                   label='flexor (swing)')
        pylab.plot(p01, [p[1] for p in periodss], 'b*',
                   label='extensor (stance)')
        pylab.plot(p01mm, [cost_function.halbertsma_x1_sw(p) for p in p01mm],
                   'r', label='halbertsma')
        pylab.plot(p01mm, [cost_function.halbertsma_x2_su(p) for p in p01mm],
                   'r')
        pylab.xlim(xlims)
        pylab.ylim(ylims)
        pylab.xlabel('Step time, s')
        pylab.ylabel('Phase time, s')
        pylab.legend(loc='upper left')

        pylab.subplot(1, 2, 2)
        pylab.title('right')
        pylab.plot(p23, [p[2] for p in periodss], 'g*',
                   label='flexor (swing)')
        pylab.plot(p23, [p[3] for p in periodss], 'b*',
                   label='extensor (stance)')
        pylab.plot(p23mm, [cost_function.halbertsma_x1_sw(p) for p in p23mm],
                   'r', label='halbertsma')
        pylab.plot(p23mm, [cost_function.halbertsma_x2_su(p) for p in p23mm],
                   'r')
        pylab.xlim(xlims)
        pylab.ylim(ylims)
        pylab.xlabel('Step time, s')
        pylab.ylabel('Phase time, s')
        pylab.legend(loc='upper left')

        pylab.savefig(n2fn('periods'))

    if plot_speedvsinput:
        pylab.figure(**meta.FIGARGS)
        pylab.title('Produced speed vs input')
        ur = (min(us), max(us))

        v01 = [cost_function.goslow_fcd2speed(p[0]+p[1])
               for p in periodss
               if p[0]+p[1] > 0]
        us01 = [i for i, p in zip(us, periodss) if p[0]+p[1] > 0]
        # print('us01', us01)
        # print('v01', v01)
        pylab.plot(us01, v01, 'ro', label='left')
        if len(v01) > 1:
            lv01s, lv01i, lv01r, lv01p, lv01std = scipy.stats.linregress(us01, v01)
            lv01 = lambda x: lv01i + lv01s*x
            pylab.plot(ur, [lv01(u) for u in ur], 'r')
            pylab.annotate('left\nr^2: {}\np: {}'.format(lv01r**2, lv01p),
                           xy=(ur[0]*1.2, max(v01)*0.9))

        v23 = [cost_function.goslow_fcd2speed(p[2]+p[3])
               for p in periodss
               if p[2]+p[3] > 0]
        us23 = [i for i, p in zip(us, periodss) if p[2]+p[3] > 0]
        # print('us23', us23)
        # print('v23', v23)
        pylab.plot(us23, v23, 'ko', label='right')
        if len(v23) > 1:
            lv23s, lv23i, lv23r, lv23p, lv23std = scipy.stats.linregress(us23, v23)
            lv23 = lambda x: lv23i + lv23s*x
            pylab.plot(ur, [lv23(u) for u in ur], 'k')
            pylab.annotate('right\nr^2: {}\np: {}'.format(lv23r**2, lv23p),
                           xy=(ur[1]*0.8, min(v23)*1.1))

        pylab.plot(ur, ur, 'b', label='identity')

        pylab.legend()

        pylab.xlim(xmin=0)
        pylab.ylim(ymin=0)

        pylab.xlabel('Control signal u, au (desired speed, m/s)')
        pylab.ylabel('Produced speed, m/s')

        pylab.savefig(n2fn('speed vs input'))

    if run_costfunction:
        cost_function.cost_function(periodss, overlapss, verbose=True)
