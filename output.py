import math
import os
try:
    import cPickle as pickle
except:
    import pickle
try:
    import pylab
except:
    pylab = None
import numpy
from scipy.spatial import distance
import scipy.stats

from simsimpy import other
import cpgpy
import symmetrical
import cost_function

FIGARGS = {
    'figsize': (16., 9.),
    'dpi': 100
}


############################### FILE ##########################################
class Callback(object):
    """"Callback and storage for optimal search.

    Contains additional functions for output, file backup and distance
    measurements between solutions.
    """
    def __init__(self, file_bak=False, fname=None, dirname=None, scale=None):
        self.storage = []
        self.scale = None if scale is None else numpy.array(scale)
        if file_bak:
            self.file_bak_setup(fname=fname, dirname=dirname)
            if not os.path.exists(dirname):
                os.mkdir(dirname)
        self.verbose = True

        self.print_n_best = None

    def __len__(self):
        return len(self.storage)

    def __contains__(self, key):
        return list(key) in [i[0] for i in self.storage]

    def file_bak_setup(self, fname=None, dirname=None):
        if fname is None:
            self.fname = str(os.getpid()) + 'bak_cb.pcl'
        else:
            self.fname = fname
        if dirname is not None:
            self.fname = os.path.join(dirname, self.fname)

    def file_bak_off(self):
        self.fname = None

    def __call__(self, x, f, accept=None):
        # print accept, x, f
        try:
            if self.scale is not None:
                x = self.scale * x
            self.storage.append([list(x), f])
        except TypeError:
            pass
        if self.verbose:
            print 'pid: %r, x: \n%r, f: %r' % (os.getpid(), x, f)
        self.dump()

    def dump(self):
        if hasattr(self, 'fname') and self.fname is not None:
            with open(self.fname, 'w') as f:
                pickle.dump(self.storage, f)

    def sort(self, reverse=True):
        self.storage.sort(key=lambda point: point[1], reverse=reverse)

    def load_storage(self, storage=None, fname=None):
        if storage is not None:
            self.storage = storage
        elif fname is not None:
            with open(fname) as f:
                self.storage = pickle.load(f)
        else:
            raise ValueError('Have to supply storage or fname.')

    def best_result(self):
        self.sort()
        return list(self.storage[-1][0])

    def add_file(self, fname):
        with open(fname) as f:
            a = pickle.load(f)
        for i in a:
            self.storage.append([i[0], i[1]])

    def find_distances_ones(self):
        self.sort()
        dists = []
        for i in xrange(len(self.storage)-1):
            dists.append(distance.euclidean(
                self.storage[i][0], self.storage[i+1][0]))
        return dists

    def remove_big(self, cutoff=10000):
        self.storage = [i for i in self.storage if i[1] <= cutoff]

    def pop_same_ones(self):
        dists = self.find_distances_ones()
        dists.reverse()
        for i, d in enumerate(dists):
            if abs(d) < 1e-3:
                self.storage.pop(len(dists) - i)
                if self.verbose:
                    print 'Popped %d' % (len(dists) - i)

    def cute_point_str(self, point):
        return (' {:1.3f}'*len(point)).format(*[round(i, 3) for i in point])

    def cute_print(self):
        self.sort()
        pp = (self.storage if self.print_n_best is None
              else self.storage[-self.print_n_best:])
        answ = ''
        for point in pp:
            answ += str(point[1]) + self.cute_point_str(point[0]) + '\n'
        return answ

    def __str__(self):
        answ = '%d mimimums found in total.\n' % len(self.storage)
        self.sort()
        pp = (self.storage if self.print_n_best is None
              else self.storage[-self.print_n_best:])
        for point in pp:
            answ += 'f = %f\n' % point[1]
            answ += '\tx:' + self.cute_point_str(point[0]) + '\n'
            answ += '\tDistance from optimal: \n\t   '
            answ += str(round(
                distance.euclidean(self.storage[-1][0], point[0]), 2))
            answ += ' ' + str([round(self.storage[-1][0][i] - point[0][i], 2)
                               for i in xrange(len(point[0]))]) + '\n\n'
        return answ


def concatenate_output_files(dirname, files, final=False):
    """Concatenate output files."""
    cb = Callback()
    cb.verbose = False

    if files == 'all':
        files = [os.path.join(dirname, f) for f in os.listdir(dirname)
                 if os.path.isfile(os.path.join(dirname, f))]
    else:
        files = [os.path.join(dirname, str(i) + 'bak_cb.pcl') for i in files]

    if final:
        final_fpath = os.path.join(dirname, 'final.pcl')
        if os.path.exists(final_fpath) and not final_fpath in files:
            files.append(final_fpath)
        else:
            print 'Warning! Final file not found.'
    print 'Uniting', files

    for f in files:
        cb.add_file(f)
    print cb
    cb.pop_same_ones()
    cb.remove_big()
    cb.file_bak_setup(fname='final.pcl', dirname=dirname)
    cb.dump()

    return cb


################################ PLOT #########################################

def plot_point(point, us=None, descrip=None, dir_output=None, comment=None,
               verbose=False):
    if us is None:
        us = cost_function.US()
    if descrip is None:
        descrip = symmetrical.DESCRIPTION

    sim = cpgpy.Simulator(cpgpy.Cpg_Numerical())
    sim.set_preset('zeros')
    sim.x_start = [0.]*4
    sim.set_point(point, descrip)

    sim.us = us
    sim.dt = 1e-4

    sim.show_p(cost_function.halbertsma_x1_sw, cost_function.halbertsma_x2_su,
               dir_output=dir_output,
               cost_function=cost_function.cost_function, comment=comment,
               verbose=verbose)


def _rearrange_peridos(periods):
    periods1 = [i[0] for i in periods]
    periods2 = [i[1] for i in periods]
    periods3 = [i[2] for i in periods]
    periods4 = [i[3] for i in periods]
    return [periods1, periods2, periods3, periods4]


def rsquare(x, y, f):
    avg = sum(y) / (len(y)-1)
    sstot = sum((i - avg)**2 for i in y)
    ssres = sum((f(x[i]) - y[i])**2 for i in xrange(len(x)))
    ssreg = sum((f(x[i]) - avg)**2 for i in xrange(len(x)))
    print sstot, ssres, ssreg
    print 1. - ssres/sstot, ssreg/sstot
    return 1. - ssres/sstot


def paper_fourth_figure(dirname, get_periods, nus=25, nbest=0):
    """Makes the fourth figure for the paper.

    Calculates periods analytically, then plots fully cycle duration vs partial
    for the solution and halbertsma's relationship.

    Plots imput signal vs produced speed.
    """
    if pylab is None:
        return None
    fname_trans = lambda fname: os.path.join(dirname, str(os.getpid()) + fname)

    cb = Callback()
    cb.verbose = False
    cb.load_storage(fname=os.path.join(dirname, 'final.pcl'))
    cb.sort(reverse=False)
    point = cb.storage[nbest][0]

    us = cost_function.US(N=nus)
    # periods1_a, periods2_a, periods3_a, periods4_a = get_periods_a(
    #     us, 1e-9, preset=[point, symmetrical.DESCRIPTION])

    periods = get_periods(point, us, symmetrical.DESCRIPTION)
    periods1, periods2, periods3, periods4 = _rearrange_peridos(periods)

    # for i in xrange(len(periods1_a)):
    #     print periods1_a[i] - periods3_a[i],
    # print

    # for i in xrange(len(periods2_a)):
    #     print periods2_a[i] - periods4_a[i],
    # print

    full_cycle_duration12 = [i[0] + i[1] for i in periods]
    full_cycle_duration34 = [i[2] + i[3] for i in periods]

    sw12 = [cost_function.halbertsma_x1_sw(i) for i in full_cycle_duration12]
    su12 = [cost_function.halbertsma_x2_su(i) for i in full_cycle_duration12]

    su34 = [cost_function.halbertsma_x2_su(i) for i in full_cycle_duration34]
    sw34 = [cost_function.halbertsma_x1_sw(i) for i in full_cycle_duration34]

    # pylab.figure(**FIGARGS)
    # pylab.plot(full_cycle_duration12, su12, '-', label='su12_halb')
    # pylab.plot(full_cycle_duration12, periods1, '*', label='periods1')
    # pylab.plot(full_cycle_duration12, sw12, '-', label='sw12_halb')
    # pylab.plot(full_cycle_duration12, periods2, '*', label='periods2')
    # pylab.plot(full_cycle_duration34, su34, '-', label='su34_halb')
    # pylab.plot(full_cycle_duration34, periods3, '*', label='periods3')
    # pylab.plot(full_cycle_duration34, sw34, '-', label='sw34_halb')
    # pylab.plot(full_cycle_duration34, periods4, '*', label='periods4')
    # pylab.ylim(ymin=0)
    # pylab.xlabel('Tc, s')
    # pylab.ylabel('T subcycle, s')
    # pylab.legend()
    # pylab.savefig(fname_trans('subcycle_vs_cycle.png'))
    # pylab.savefig(fname_trans('subcycle_vs_cycle.pdf'), format='pdf')

    speeds = [cost_function.goslow_fcd2speed(i) for i in full_cycle_duration12]
    print 'rsq value:', rsquare(full_cycle_duration12, speeds, lambda x: x)
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(
        us, speeds)
    print 'line: {}x + {}, r: {}, r^2: {}, p: {}, std_err: {}'.format(
        slope, intercept, r_value, r_value**2, p_value, std_err)
    linreg = lambda x: slope * x + intercept
    # for i in xrange(len(full_cycle_duration12)):
    #     print i, full_cycle_duration12[i], speeds[i], linreg

    pylab.figure(**FIGARGS)
    rng = (0, 1.2)
    pylab.plot(us, speeds, '.', color='#800080', markersize=15, label='12')
    # pylab.plot(rng, rng, color='#800080')
    pylab.plot(us, [linreg(i) for i in us], color='#C000C0')
    # pylab.plot(us, [cost_function.goslow_fcd2speed(i)
    #                 for i in full_cycle_duration34],
    #            '.', markersize=15, label='34')
    pylab.xlim(rng)
    pylab.ylim(ymin=0)
    pylab.xlabel('us, au')
    pylab.ylabel('speed, m/s')
    # pylab.legend()
    pylab.savefig(fname_trans('speed_vs_input.png'))
    pylab.savefig(fname_trans('speed_vs_input.pdf'), format='pdf')

    pylab.show()


########################## WRAPPERS ###########################################
def wrap_concatenate_files(n=3, dirname=None, files=None):
    """Wrapper for concatenate output files and plot the best."""
    if dirname is None:
        dirname = 'result13'
    if files is None:
        files = 'all'
    cb = concatenate_output_files(dirname, files, final=True)

    cb.print_n_best = n
    print cb.cute_print()

    plot_best_results(n=n, cb=cb, dirname=dirname)


def plot_best_results(n=3, cb=None, dirname=None):
    """Plot best ones"""
    if dirname is None:
        dirname = 'result5'
    if cb is None:
        cb = Callback()
        cb.verbose = False
        cb.add_file(os.path.join(dirname, 'final.pcl'))
    cb.sort(reverse=False)

    for i in cb.storage[:n]:
        print 'f =', i[1]
        loc_wrap = lambda: plot_point(i[0], dir_output=dirname, comment=i[1],
                                      verbose=True)
        loc_wrap()

    print 'Done!'


if __name__ == '__main__':
    wrap_concatenate_files(n=2, dirname='result15',
                           files='all')
