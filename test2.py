#!/usr/bin/env python3
import math
import warnings
import numpy
import pylab

import cpgpy
import simulator


warnings.filterwarnings('error')
vector_repr = [
0.20322228, 0.15039054,
1.7361355 , 3.51094074, 4.98069952,
2.09416532, 0.29899163,
1.50392213, -2.65231136,
-0.23240355, 4.67104871]

a1 = 1.50392213
a4 = -2.65231136
a2 = 3.51094074
a3 = -0.0823

xval = 1.
x01 = 0.20237236
x02 = 0.15001375

xi1 = -0.23240355
xi2 = 4.67104871

gu1 = 2.09416532
gu2 = 0.29899163

detA = a1*a4 - a2*a3

ai1 = a4 / detA
ai4 = a1 / detA
ai2 = -a2 / detA
ai3 = -a3 / detA

us = numpy.linspace(0.111, 1.155, 5)

d5 = (-a3*xi1 + a1*xi2) / detA

s = (a1 + a4) / 2
st = (a1 - a4) / 2
q = st**2 + a2*a3

z2 = st*ai3*xi1 + a2*ai3*xi2 + a3*(ai4*xi1 + x01) - st*(ai4*xi2 + x02)

ac1 = (-a3*gu1 + a1*gu2)*(a1*a4 + a2*a3)/detA

a = xval*s**2 - x02*q - a3*xi1 + a1*xi2
b = z2 + (xval+d5)*s
c = xval - x02

u2t = lambda u: (b + gu2*u - math.sqrt((b + gu2*u)**2 - 2*(a + ac1*u)*c)) / (a + ac1*u)

# h0 = gu2
# h1 = gu2**2
# h2 = b*gu2 - ac1*c
# h3 = b**2 - 2*a*c
# h4 = ac1
# u2t2 = lambda u: (b + gu2*u) / (a + ac1*u) + math.sqrt(((b + gu2*u)**2 - 2*(a + ac1*u)*c)/ (a + ac1*u)**2)

t2v = lambda T: 0.3585/T**1.68

ts = [u2t(ui) for ui in us]
# ts2 = [u2t2(ui) for ui in us]
# pylab.figure()
# pylab.plot(us, ts, label='org')
# pylab.plot(us, ts2, label='appr')
# pylab.legend()

t12fp = lambda x: (0.168 + x) / 0.9062
t22fp = lambda x: (-0.168 + x) / 0.0938
fp2t2 = lambda x: 0.168 + 0.0938*x
fp2t1 = lambda x: -0.168 + 0.9062*x

fps = [t12fp(i) for i in ts]
t2s = [fp2t2(i) for i in fps]
fpsec = [i+j for i, j in zip(ts, t2s)]

vs = [t2v(i) for i in fps]

# vector_repr = [0., 0., a2, a3, 0.981, gu1, gu2, a1, a4, xi1, xi2]
# ts_simd, periodss_simd, overlapss = simulator.get_periods_vs_input(
#     'analytical', 'symmetrical', vector_repr, us, active=[True, False, False, True])
# print(ts_simd)
# print(periodss_simd)
# print(vector_repr)
# ts_simd = [i[2]+i[3] for i in periodss_simd]
# vs_simd = [t2v(i) for i in ts_simd]
cpg = cpgpy.Cpg_Analytical()
ps = simulator.CpgParamSetter('symmetrical')
ts1 = []
ts2 = []
for u in us:
    cpg.active = [True, False, False, True]
    cpg.reset()
    ps(cpg, vector_repr)
    cpg.u = [u]*4
    ts1.append(cpg.step_to_change())
    print(cpg.change)
    cpg.step_to_change()
    print(cpg.change)

pylab.figure()
pylab.subplot(3, 1, 1)
pylab.plot(us, ts, label='ts')
pylab.plot(us, fps, label='fps')
pylab.plot(us, t2s, label='t2s')
pylab.plot(us, ts1, label='simd t1')
pylab.xlabel('input, au')
pylab.ylabel('periods, s')
pylab.xlim([0., 1.2])
pylab.ylim([0., 0.8])
pylab.legend()

pylab.subplot(3, 1, 2)
pylab.plot(fps, ts, label='t1')
pylab.plot(fps, t2s, label='t2')
pylab.plot(fps, fpsec, label='full')
pylab.xlabel('full period time, s')
pylab.ylabel('partial period, s')
pylab.xlim([0., .8])
pylab.ylim([0., .8])
pylab.legend()

pylab.subplot(3, 1, 3)
pylab.plot(us, vs, label='speed')
pylab.plot(us, us, label='ident')
# pylab.plot(us, vs_simd, label='speed simd')
pylab.xlabel('input, au')
pylab.ylabel('speed, m/s')
pylab.xlim([0., 1.2])
pylab.ylim([0., 1.2])
pylab.legend()


pylab.show()
