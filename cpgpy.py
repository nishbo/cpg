#!/usr/bin/env python3
import sys
import math

import numpy
import scipy
import scipy.optimize as optimize
import scipy.linalg as linalg
import pylab

from simsimpy import intstep, other


sign = lambda x: 1 if x>0 else (-1 if x<0 else 0)


def init_cpg(cpg_type):
    if cpg_type == 'analytical':
        return Cpg_Analytical()
    elif cpg_type == 'numerical':
        return Cpg_Numerical()
    else:
        raise ValueError('Only analytical or numerical CPG supported')


class Cpg_Core(object):
    """Core parameters of a cpg.

    If it is number, it starts from 1, if it is index, it starts from 0.
    x1 = x[0], x2 = x[1], x3 = x[2], x4 = x[3]
    u1 = u[0], u2 = u[1], u3 = u[2], u4 = u[3]
    u0 is constant input (x0 in Sergiy's paper)
    flexor extensor flexor extensor

    For walking, no connection between flexors =>
        r[0][2] = r[2][0] = 0

    Children must override step and step_to_change
    """
    def __init__(self):
        self.r = numpy.zeros((4, 4))
        self.g_u = [0.]*4
        self.leak = [0.]*4
        self.u0 = [0.]*4
        self.dt = 1e-4

        self.tmax = 5
        self.flush()

    def get_active_numbers(self):
        return [int(self.active[1]), 2 + int(self.active[3])]

    def flush(self):
        self._x = [0.]*4
        self.u = [0.]*4
        self.active = [True, False, False, True]
        self.change = [False]*2
        self.reset()

    def x_rs(self, x=None):
        if x is None:
            x = self._x
        return [int(self.active[i]) * (
            self.u0[i]
            + self.g_u[i] * self.u[i]
            + self.leak[i] * x[i]
            + sum(self.r[i][j] * x[j] for j in range(4)))
            for i in range(4)]

    def step(self):
        raise NotImplemented('Dummy function. You should not see this.')

    def step_to_change(self):
        raise NotImplemented('Dummy function. You should not see this.')

    def walk_full_step(self):
        self.step_to_change()  # we probably did not start at 0

        step_start = [0.]*4
        periods = [0.]*4
        overlaps = {'ff': 0., 'fe': 0., 'ee': 0.}
        t = 0.
        previous_an = self.get_active_numbers()
        while 0 in periods:
            tau = self.step_to_change()
            if tau < 0:
                break
            t += tau

            # new active numbers
            an = self.get_active_numbers()
            # identify which steps started
            started_ids = [i for i in an if i not in previous_an]
            for si in started_ids:
                if step_start[si] <= 0:
                    step_start[si] = t

            # identify which steps ended
            ended_ids = [i for i in previous_an if i not in an]
            for ei in ended_ids:
                if step_start[ei] > 0 and periods[ei] <= 0.:
                    periods[ei] = t - step_start[ei]

            # print(previous_an, an, started_ids, ended_ids)

            # calculate overlaps, based on previous activity
            prefiously_active = ''
            if 0 in previous_an or 2 in previous_an:
                # had a flexor active
                prefiously_active += 'f'
            if 1 in previous_an or 3 in previous_an:
                prefiously_active += 'e'
            if prefiously_active == 'f':
                overlaps['ff'] += tau
            elif prefiously_active == 'fe':
                overlaps['fe'] += tau
            elif prefiously_active == 'e':
                overlaps['ee'] += tau

            if t > self.tmax:
                break
            # print(self.change, self.active, step_start, periods, self.x)
            previous_an = an

        return (t, periods, overlaps)

    def step_to_change_accumulative(self):
        ts = [0]
        xs = [list(self.x)]
        self.change = [False]*2
        while ts[-1] < self.tmax:
            x_old = list(self.x);
            ts.append(ts[-1]+self.dt)
            xs.append(self.step())
            if (any(i<j for i, j in zip(xs[-1], x_old)) or
                    True in self.change):
                break
        return (ts, xs)

    def reset(self):
        pass

    def walk_full_step_using_step(self):
        # self.step_to_change()  # we probably did not start at 0
        ts, xs = self.step_to_change_accumulative()

        step_start = [0.]*4
        periods = [0.]*4
        overlaps = {'ff': 0., 'fe': 0., 'ee': 0.}
        t = 0.
        previous_an = self.get_active_numbers()
        while 0 in periods:
            tsl, xsl = self.step_to_change_accumulative()
            tau = tsl[-1]  # for analysis
            tsl = [tl + ts[-1] for tl in tsl]  # shift from 0 to global time
            # add to storage:
            ts += tsl
            xs += xsl

            if tau < 0:
                break
            t += tau

            # new active numbers
            an = self.get_active_numbers()
            # identify which steps started
            started_ids = [i for i in an if i not in previous_an]
            for si in started_ids:
                if step_start[si] <= 0:
                    step_start[si] = t

            # identify which steps ended
            ended_ids = [i for i in previous_an if i not in an]
            for ei in ended_ids:
                if step_start[ei] > 0 and periods[ei] <= 0.:
                    periods[ei] = t - step_start[ei]

            # calculate overlaps, based on previous activity
            prefiously_active = ''
            if 0 in previous_an or 2 in previous_an:
                # had a flexor active
                prefiously_active += 'f'
            if 1 in previous_an or 3 in previous_an:
                prefiously_active += 'e'
            if prefiously_active == 'f':
                overlaps['ff'] += tau
            elif prefiously_active == 'fe':
                overlaps['fe'] += tau
            elif prefiously_active == 'e':
                overlaps['ee'] += tau

            if t > self.tmax:
                break
            # print(self.change, self.active, step_start, periods)
            previous_an = an

        return (t, periods, overlaps, ts, xs)

    def walk_two_full_steps_using_step(self):
        t, periods, overlaps, ts, xs = self.walk_full_step_using_step()
        tD, periodsD, overlapsD, tsD, xsD = self.walk_full_step_using_step()
        tsD = [tsDi + ts[-1] for tsDi in tsD]  # shift from 0 to global time
        return (t+tD, periods, overlaps, ts+tsD, xs+xsD)


    def __str__(self):
        return str(self._x)


class Cpg_Numerical(Cpg_Core):
    """Numerical solution for Cpg_Core"""
    def __init__(self):
        super(Cpg_Numerical, self).__init__()

    def step(self):
        self.change = [False]*2
        self._x = intstep.rk4(self._x, lambda t, *x: self.x_rs(x), 0, self.dt)
        # self._x = intstep.euler(self._x, lambda t, *x: self.x_rs(x), 0, self.dt)

        crossed = [i for i in range(4) if self.active[i] and self._x[i] >= 1]

        for i in crossed:
            id1 = (i // 2) * 2
            id2 = (i // 2) * 2 + 1
            self.active[id1], self.active[id2] = (
                self.active[id2], self.active[id1])
            assert self.active[id1] != self.active[id2]
            self._x[id1] = self._x[id2] = 0.
            self.change[i // 2] = True

        return list(self._x)

    def x():
        doc = "Value of x"

        def fget(self):
            return self._x

        def fset(self, value):
            self._x = value

        return locals()
    x = property(**x())

    def step_to_change(self):
        tau = 0.
        self.change = [False]*2
        # ts = []
        # xs = []
        while tau < self.tmax:
            tau += self.dt
            x_old = list(self._x)
            x_new = self.step()
            # ts.append(tau)
            # xs.append(self.x)
            if True in self.change:
                # pylab.subplot(2, 1, 1)
                # pylab.plot(ts, [i[0] for i in xs], label='1')
                # # pylab.plot(ts, [i[1] for i in xs], label='2')
                # pylab.plot(ts, [i[2] for i in xs], label='3')
                # # pylab.plot(ts, [i[3] for i in xs], label='4')
                # pylab.legend()

                # pylab.subplot(2, 1, 2)
                # pylab.plot(ts, [self.x_rs(x=i)[0] for i in xs], label='1')
                # # pylab.plot(ts, [self.x_rs(x=i)[1] for i in xs], label='2')
                # pylab.plot(ts, [self.x_rs(x=i)[2] for i in xs], label='3')
                # # pylab.plot(ts, [self.x_rs(x=i)[3] for i in xs], label='4')
                # pylab.legend()

                # pylab.show()
                return tau
            if any(i<j for i, j in zip(x_new, x_old)):
                return -1
        return -1


class Cpg_Analytical(Cpg_Core):
    """Has analytical ways to calculate periods."""
    def __init__(self, solution_type='s'):
        super(Cpg_Analytical, self).__init__()
        self._t = 0.  # relative last change kinda
        self._A = None
        self._A_inv = None
        self._B = None
        self._x0 = None

        self._solutions = {
            'l': [self._gtoc_l_1, self._gtoc_l_2],
            's': [self._gtoc_s_1, self._gtoc_s_2],
            'sn': [self._gtoc_sn_1, self._gtoc_sn_2],
            'c': [self._gtoc_c_1, self._gtoc_c_2],
            'q': [self._gtoc_q_1, self._gtoc_q_2],
            'penta': [self._gtoc_penta_1, self._gtoc_penta_2],
            'hexa': [self._gtoc_hexa_1, self._gtoc_hexa_2],
            'septa': [self._gtoc_septa_1, self._gtoc_septa_2],
            'octa': [self._gtoc_octa_1, self._gtoc_octa_2],
            'nona': [self._gtoc_nona_1, self._gtoc_nona_2],
            'deca': [self._gtoc_deca_1, self._gtoc_deca_2],
            'eleven': [self._gtoc_eleven_1, self._gtoc_eleven_2],
            'twelve': [self._gtoc_twelve_1, self._gtoc_twelve_2],
            'n': [self._gtoc_n_1, self._gtoc_n_2],
            'b': [self._gtoc_b_1, self._gtoc_b_2],
            'b2': [self._gtoc_b2_1, self._gtoc_b2_2]
        }
        self.get_time_of_change_1 = self._solutions[solution_type][0]
        self.get_time_of_change_2 = self._solutions[solution_type][1]

    def reset(self):
        self._t = 0.

    def set_analytical_params(self):
        """x, A, B"""
        an = self.get_active_numbers()
        self._x0 = numpy.array([self._x[an[0]], self._x[an[1]]])
        self._A = numpy.array([[self.leak[an[0]], self.r[an[0]][an[1]]],
                               [self.r[an[1]][an[0]], self.leak[an[1]]]])
        self._B = numpy.array([self.u0[an[0]] + self.g_u[an[0]]*self.u[an[0]],
                               self.u0[an[1]] + self.g_u[an[1]]*self.u[an[1]]])
        return an

    def _A_inv_func(self):
        det = self._A[0][0]*self._A[1][1] - self._A[0][1]*self._A[1][0]
        if det:
            self._A_inv = numpy.array([[self._A[1][1], -self._A[0][1]],
                                       [-self._A[1][0], self._A[0][0]]]) / det
            return self._A_inv
        else:
            self._A_inv = None
            raise numpy.linalg.linalg.LinAlgError()
        # return numpy.linalg.inv(self._A)

    # FUNCTIONS FOR GET TIME OF CHANGE
    def _D1(self, xval=1.):
        """Parts of sum(K.*D)=d5, where K is exp(At)"""
        return [self._A_inv[0][0]*self._B[0] + self._x0[0],
                self._A_inv[0][0]*self._B[1] + self._x0[1],
                self._A_inv[0][1]*self._B[0],
                self._A_inv[0][1]*self._B[1],
                xval + self._A_inv[0][0]*self._B[0] +
                self._A_inv[0][1]*self._B[1]]

    def _D2(self, xval=1.):
        """Parts of sum(K.*D)=d5, where K is exp(At)"""
        return [self._A_inv[1][0]*self._B[0],
                self._A_inv[1][0]*self._B[1],
                self._A_inv[1][1]*self._B[0] + self._x0[0],
                self._A_inv[1][1]*self._B[1] + self._x0[1],
                xval + self._A_inv[1][0]*self._B[0] +
                self._A_inv[1][1]*self._B[1]]

    def _med(self, d1, d2, d3, d4, d5):
        """Matrix exponent decomposition and linear eq"""
        # Matrix exponent decomposition
        s = (self._A[0][0] + self._A[1][1]) / 2
        st = s - self._A[1][1]
        # print(st, self._A[0][1], self._A[1][0])
        q1 = st**2 + self._A[0][1]*self._A[1][0]
        qsgn = sign(q1)
        qm = math.sqrt(qsgn*q1)

        # Combination of decomposition and linear equation
        z1 = d1 + d4
        z2 = st*d1 + self._A[0][1]*d2 + self._A[1][0]*d3 - st*d4
        z3 = d5
        return [s, st, qm, z1, z2, z3, qsgn]

    def _gtoc_l_1(self, xval=1.):
        """Linear solution"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        t = (z3 - z1) / (z2 + z3*s)
        if t > self.tmax:
            return None
        if t > 0:
            return t
        else:
            return None

    def _gtoc_l_2(self, xval=1.):
        """Linear solution"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        t = (z3 - z1) / (z2 + z3*s)
        if t > self.tmax:
            return None
        if t > 0:
            return t
        else:
            return None

    def _gtoc_s_1(self, xval=1.):
        """square approximation a*t**2 - 2b*t + 2c=0"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        a = z3*s**2 - qsgn*z1*q**2
        b = z2 + z3*s
        c = z3 - z1
        Dt = b**2 - 2*a*c
        if Dt >= 0:
            Dt = math.sqrt(Dt)
        else:
            return None
        if a:
            t = (b - Dt) / a
        elif b:
            t = c / b
        else:
            return None
        # t1, t2 = sorted([(b - Dt) / a, (b + Dt) / a])
        # if t1 > 0:
        #     t = t1
        # elif t2 > 0:
        #     t = t2
        # else:
        #     return None

        if t > self.tmax or t < 0:
            return None

        # # TESTING PURPOSES
        # if t == (b - Dt) / a:
        #     singleton.chose_minus_left += 1
        #     # singleton.increment_left = 1
        # else:
        #     singleton.chose_plus_left += 1
        #     # singleton.increment_left = 0

        # # Check
        # if q == 0:
        #     if abs(z1 + z2*t - z3*math.exp(-s*t)) > 1e-1:
        #         return None
        # else:
        #     if abs(z1*math.cosh(q*t) + z2/q*math.sinh(q*t) -
        #            z3*math.exp(-s*t)) > 1e-1:
        #         return None

        return t

    def _gtoc_s_2(self, xval=1.):
        """square approximation a*t**2 - 2b*t + 2c=0"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        a = z3*s**2 - qsgn*z1*q**2
        b = z2 + z3*s
        c = z3 - z1
        Dt = b**2 - 2*a*c
        if Dt >= 0:
            Dt = math.sqrt(Dt)
        else:
            return None
        if a:
            t = (b - Dt) / a
        elif b:
            t = c / b
        else:
            return None
        # t1, t2 = sorted([(b - Dt) / a, (b + Dt) / a])
        # if t1 > 0:
        #     t = t1
        # elif t2 > 0:
        #     t = t2
        # else:
        #     return None

        if t > self.tmax or t < 0:
            return None

        # # TESTING PURPOSES
        # if t == (b - Dt) / a:
        #     singleton.chose_minus_right += 1
        #     # singleton.increment_right = 1
        # else:
        #     singleton.chose_plus_right += 1
        #     # singleton.increment_right = 0

        # # Check
        # if q == 0:
        #     if abs(z1 + z2*t - z3*math.exp(-s*t)) > 1e-1:
        #         return None
        # else:
        #     if abs(z1*math.cosh(q*t) + z2/q*math.sinh(q*t) -
        #            z3*math.exp(-s*t)) > 1e-1:
        #         return None

        return t

    def _gtoc_sn_1(self, xval=1.):
        """square numerical approach"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p0 = (qsgn*z1*q**2 - z3*s**2) / 2
        p1 = z2 + z3*s
        p2 = z1 - z3

        roots = numpy.roots([p0, p1, p2])
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_sn_2(self, xval=1.):
        """square numerical approach"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p0 = (qsgn*z1*q**2 - z3*s**2) / 2
        p1 = z2 + z3*s
        p2 = z1 - z3

        # pylab.figure()
        # x = numpy.linspace(0, 1, 50)
        # y = [p0*i**2+p1*i+p2 for i in x]
        # p0 = (qsgn*z2*q**2 + z3*s**3) / 6
        # p1 = (qsgn*z1*q**2 - z3*s**2) / 2
        # p2 = z2 + z3*s
        # p3 = z1 - z3
        # y2 = [p0*i**3+p1*i**2+p2*i+p3 for i in x]
        # pylab.plot(x, y, label='sn')
        # pylab.plot(x, y2, label='c')
        # pylab.plot([0, 1], [0, 0], 'k', label='zero')
        # pylab.legend()
        # pylab.show()
        # sys.exit()

        roots = numpy.roots([p0, p1, p2])
        # print(roots)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_c_1(self, xval=1.):
        """cubic p0*t**3 + p1*t**2 + p2*t + p3=0"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p0 = (qsgn*z2*q**2 + z3*s**3) / 6
        p1 = (qsgn*z1*q**2 - z3*s**2) / 2
        p2 = z2 + z3*s
        p3 = z1 - z3

        roots = numpy.roots([p0, p1, p2, p3])
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_c_2(self, xval=1.):
        """cubic p0*t**3 + p1*t**2 + p2*t + p3=0"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p0 = (qsgn*z2*q**2 + z3*s**3) / 6
        p1 = (qsgn*z1*q**2 - z3*s**2) / 2
        p2 = z2 + z3*s
        p3 = z1 - z3

        roots = numpy.roots([p0, p1, p2, p3])
        # print(roots)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_q_1(self, xval=1.):
        """quadratic p0*t**4 + p1*t**3 + p2*t**2 + p3*t + p4=0"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*5
        p[0] = (qsgn*z1*q**4 - z3*s**4) / 24
        p[1] = (qsgn*z2*q**2 + z3*s**3) / 6
        p[2] = (qsgn*z1*q**2 - z3*s**2) / 2
        p[3] = z2 + z3*s
        p[4] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_q_2(self, xval=1.):
        """quadratic p0*t**4 + p1*t**3 + p2*t**2 + p3*t + p4=0"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*5
        p[0] = (qsgn*z1*q**4 - z3*s**4) / 24
        p[1] = (qsgn*z2*q**2 + z3*s**3) / 6
        p[2] = (qsgn*z1*q**2 - z3*s**2) / 2
        p[3] = z2 + z3*s
        p[4] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_penta_1(self, xval=1.):
        """penta"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*6
        p[0] = (qsgn*z2*q**4 + z3*s**5) / 120
        p[1] = (qsgn*z1*q**4 - z3*s**4) / 24
        p[2] = (qsgn*z2*q**2 + z3*s**3) / 6
        p[3] = (qsgn*z1*q**2 - z3*s**2) / 2
        p[4] = z2 + z3*s
        p[5] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_penta_2(self, xval=1.):
        """penta"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*6
        p[0] = (qsgn*z2*q**4 + z3*s**5) / 120
        p[1] = (qsgn*z1*q**4 - z3*s**4) / 24
        p[2] = (qsgn*z2*q**2 + z3*s**3) / 6
        p[3] = (qsgn*z1*q**2 - z3*s**2) / 2
        p[4] = z2 + z3*s
        p[5] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_hexa_1(self, xval=1.):
        """hexa"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*7
        p[0] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[1] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[2] = (qsgn*z1*q**4 - z3*s**4) / 24
        p[3] = (qsgn*z2*q**2 + z3*s**3) / 6
        p[4] = (qsgn*z1*q**2 - z3*s**2) / 2
        p[5] = z2 + z3*s
        p[6] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_hexa_2(self, xval=1.):
        """hexa"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*7
        p[0] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[1] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[2] = (qsgn*z1*q**4 - z3*s**4) / 24
        p[3] = (qsgn*z2*q**2 + z3*s**3) / 6
        p[4] = (qsgn*z1*q**2 - z3*s**2) / 2
        p[5] = z2 + z3*s
        p[6] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_septa_1(self, xval=1.):
        """septa"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*8
        p[0] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[1] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[2] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[3] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[4] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[5] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[6] = z2 + z3*s
        p[7] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_septa_2(self, xval=1.):
        """septa"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*8
        p[0] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[1] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[2] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[3] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[4] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[5] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[6] = z2 + z3*s
        p[7] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_octa_1(self, xval=1.):
        """octa"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*9
        p[0] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[1] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[2] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[3] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[4] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[5] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[6] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[7] = z2 + z3*s
        p[8] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_octa_2(self, xval=1.):
        """octa"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*9
        p[0] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[1] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[2] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[3] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[4] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[5] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[6] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[7] = z2 + z3*s
        p[8] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_nona_1(self, xval=1.):
        """nona"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*10
        p[0] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[1] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[2] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[3] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[4] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[5] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[6] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[7] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[8] = z2 + z3*s
        p[9] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_nona_2(self, xval=1.):
        """nona"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*10
        p[0] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[1] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[2] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[3] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[4] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[5] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[6] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[7] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[8] = z2 + z3*s
        p[9] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_deca_1(self, xval=1.):
        """deca"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*11
        p[0] = (qsgn*z1*q**10 - z3*s**10) / math.factorial(10)
        p[1] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[2] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[3] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[4] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[5] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[6] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[7] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[8] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[9] = z2 + z3*s
        p[10] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_deca_2(self, xval=1.):
        """deca"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*11
        p[0] = (qsgn*z1*q**10 - z3*s**10) / math.factorial(10)
        p[1] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[2] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[3] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[4] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[5] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[6] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[7] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[8] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[9] = z2 + z3*s
        p[10] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_eleven_1(self, xval=1.):
        """eleven"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*12
        p[0] = (qsgn*z2*q**10 + z3*s**11) / math.factorial(11)
        p[1] = (qsgn*z1*q**10 - z3*s**10) / math.factorial(10)
        p[2] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[3] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[4] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[5] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[6] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[7] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[8] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[9] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[10] = z2 + z3*s
        p[11] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_eleven_2(self, xval=1.):
        """eleven"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*12
        p[0] = (qsgn*z2*q**10 + z3*s**11) / math.factorial(11)
        p[1] = (qsgn*z1*q**10 - z3*s**10) / math.factorial(10)
        p[2] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[3] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[4] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[5] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[6] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[7] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[8] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[9] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[10] = z2 + z3*s
        p[11] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_twelve_1(self, xval=1.):
        """twelve"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*13
        p[0] = (qsgn*z1*q**12 - z3*s**12) / math.factorial(12)
        p[1] = (qsgn*z2*q**10 + z3*s**11) / math.factorial(11)
        p[2] = (qsgn*z1*q**10 - z3*s**10) / math.factorial(10)
        p[3] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[4] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[5] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[6] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[7] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[8] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[9] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[10] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[11] = z2 + z3*s
        p[12] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None

    def _gtoc_twelve_2(self, xval=1.):
        """twelve"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        p = [0]*13
        p[0] = (qsgn*z1*q**12 - z3*s**12) / math.factorial(12)
        p[1] = (qsgn*z2*q**10 + z3*s**11) / math.factorial(11)
        p[2] = (qsgn*z1*q**10 - z3*s**10) / math.factorial(10)
        p[3] = (qsgn*z2*q**8 + z3*s**9) / math.factorial(9)
        p[4] = (qsgn*z1*q**8 - z3*s**8) / math.factorial(8)
        p[5] = (qsgn*z2*q**6 + z3*s**7) / math.factorial(7)
        p[6] = (qsgn*z1*q**6 - z3*s**6) / math.factorial(6)
        p[7] = (qsgn*z2*q**4 + z3*s**5) / math.factorial(5)
        p[8] = (qsgn*z1*q**4 - z3*s**4) / math.factorial(4)
        p[9] = (qsgn*z2*q**2 + z3*s**3) / math.factorial(3)
        p[10] = (qsgn*z1*q**2 - z3*s**2) / math.factorial(2)
        p[11] = z2 + z3*s
        p[12] = z1 - z3

        roots = numpy.roots(p)
        rootsreal = [numpy.real(r) for r in roots if numpy.isreal(r) and r > 0]
        if len(rootsreal) > 0:
            t = min(rootsreal)
            if t < self.tmax:
                return t
            else:
                return None
        else:
            return None


    def _gtoc_n_1(self, xval=1.):
        """Numerical solution"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_1(xval=xval)
        if self._x0[0] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D1(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        if qsgn == 0:
            eq = lambda tt: z1 + z2*tt - z3*math.exp(-s*tt)
        elif qsgn < 0:
            eq = lambda tt: (z1*math.cos(q*tt) + z2/q*math.sin(q*tt) -
                             z3*math.exp(-s*tt))
        else:
            eq = lambda tt: (z1*math.cosh(q*tt) + z2/q*math.sinh(q*tt) -
                             z3*math.exp(-s*tt))

        if eq(0) >= 0:
            find_bound = lambda r: optimize.fminbound(
                eq, 0, r, xtol=1e-2, maxfun=400)
        else:
            find_bound = lambda r: optimize.fminbound(
                lambda x: -eq(x), 0, r, xtol=1e-2, maxfun=400)

        t = None
        tmax = self.tmax
        ctr1 = 0
        while True:
            rb = find_bound(tmax)
            ctr2 = 0
            while True:
                rbo = rb
                rb = find_bound(rb)
                ctr2 += 1
                if abs(rb - rbo) < 1e-2 or ctr2 > 20:
                    break
            if eq(0) * eq(rb) > 0:
                break
            else:
                try:
                    t = optimize.brentq(eq, 0, rb, xtol=1e-5, maxiter=400)
                    tmax = t
                except Exception as m:
                    # print('here1', eq(0), eq(rb))

                    # x = pylab.linspace(0, rb, 500)
                    # y = [eq(i) for i in x]
                    # y2 = [self._an_rs_separate_1(t=i) for i in x]
                    # # print([i for i in y if i > 0])
                    # pylab.figure()
                    # pylab.plot(x, y, label='eq')
                    # pylab.plot(x, y2, label='rs')
                    # pylab.legend()
                    # pylab.show()


                    # raise m
                    return None
            ctr1 += 1
            if ctr1 > 20:
                break
        return t

    def _gtoc_n_2(self, xval=1.):
        """Numerical solution"""
        self.update_params()
        if self._A_inv is None:
            return self._gtoc_singular_2(xval=xval)
        if self._x0[1] >= xval:
            return self.dt
        d1, d2, d3, d4, d5 = self._D2(xval=xval)
        s, st, q, z1, z2, z3, qsgn = self._med(d1, d2, d3, d4, d5)

        if qsgn == 0:
            eq = lambda tt: z1 + z2*tt - z3*math.exp(-s*tt)
        elif qsgn < 0:
            eq = lambda tt: (z1*math.cos(q*tt) + z2/q*math.sin(q*tt) -
                             z3*math.exp(-s*tt))
        else:
            eq = lambda tt: (z1*math.cosh(q*tt) + z2/q*math.sinh(q*tt) -
                             z3*math.exp(-s*tt))

        if eq(0) >= 0:
            find_bound = lambda r: optimize.fminbound(
                eq, 0, r, xtol=1e-2, maxfun=400)
        else:
            find_bound = lambda r: optimize.fminbound(
                lambda x: -eq(x), 0, r, xtol=1e-2, maxfun=400)

        t = None
        tmax = self.tmax
        ctr1 = 0
        while True:
            rb = find_bound(tmax)
            ctr2 = 0
            while True:
                rbo = rb
                rb = find_bound(rb)
                ctr2 += 1
                if abs(rb - rbo) < 1e-2 or ctr2 > 20:
                    break
            # print(rb, eq(0), eq(rb))
            # pylab.figure()
            # x = pylab.linspace(0, rb, 50)
            # y = [-eq(i) for i in x]
            # pylab.plot(x, y)
            # pylab.show()
            # sys.exit()
            if eq(0) * eq(rb) > 0:
                break
            else:
                try:
                    t = optimize.brentq(eq, 0, rb, xtol=1e-5, maxiter=400)
                    tmax = t
                except Exception as m:
                    # print('here2', eq(0), eq(rb))

                    # x = pylab.linspace(0, rb, 500)
                    # y = [eq(i) for i in x]
                    # y2 = [self._an_rs_separate_2(t=i) for i in x]
                    # # print([i for i in y if i > 0])
                    # pylab.figure()
                    # pylab.plot(x, y, label='eq')
                    # pylab.plot(x, y2, label='rs')
                    # pylab.legend()
                    # pylab.show()


                    # raise m
                    return None
            ctr1 += 1
            if ctr1 > 20:
                break
        return t

    def _gtoc_b_1(self, xval=1.):
        """Brute numerical solution"""
        self.update_params()
        eq = lambda tt: self._an_rs_separate_1(t=tt)-1.
        # t = optimize.brentq(eq, 0, 2, xtol=1e-5, maxiter=400)
        try:
            t = optimize.brentq(eq, 0, 2, xtol=1e-5, maxiter=400)
        except:
            return None
        return t

    def _gtoc_b_2(self, xval=1.):
        """Brute numerical solution"""
        self.update_params()
        eq = lambda tt: self._an_rs_separate_2(t=tt)-1.
        try:
            t = optimize.brentq(eq, 0, 2, xtol=1e-5, maxiter=400)
        except:
            return None
        return t

    def _gtoc_b2_1(self, xval=1.):
        """Brute numerical solution"""
        self.update_params()
        eq = lambda tt: self._analytical_rs(tt)[0]-1.
        # t = optimize.brentq(eq, 0, 2, xtol=1e-5, maxiter=400)
        try:
            t = optimize.brentq(eq, 0, 2, xtol=1e-5, maxiter=400)
        except:
            return None
        return t

    def _gtoc_b2_2(self, xval=1.):
        """Brute numerical solution"""
        self.update_params()
        eq = lambda tt: self._analytical_rs(tt)[1]-1.
        try:
            t = optimize.brentq(eq, 0, 2, xtol=1e-5, maxiter=400)
        except:
            return None
        return t

    def _gtoc_singular(self, xval=[1., 1.]):
        """Replaces estimations in case of singular A"""
        return [self._gtoc_singular_1(xval[0]),
                self._gtoc_singular_2(xval[1])]

    def get_time_of_change_1(self, xval=1.):
        """Uses various solution methods to find the desired value"""
        raise NotImplemented('Dummy function. You should not see this.')

    def get_time_of_change_2(self, xval=1.):
        """Uses various solution methods to find the desired value"""
        raise NotImplemented('Dummy function. You should not see this.')

    def _gtoc_singular_1(self, xval=1.):
        if self._x0[0] >= xval:
            return self.dt
        if self._B[0] == 0:
            return None
        else:
            return (xval - self._x0[0]) / self._B[0]

    def _gtoc_singular_2(self, xval=1.):
        if self._x0[1] >= xval:
            return self.dt
        if self._B[1] == 0:
            return None
        else:
            return (xval - self._x0[1]) / self._B[1]

    # RIGTH SIDE ESTIMATION
    def my_expm(self, t):
        a1, a2 = self._A[0][0], self._A[0][1]
        a3, a4 = self._A[1][0], self._A[1][1]
        s = (a1+a4) / 2
        st = (a1-a4) / 2

        q1 = st**2 + a2*a3
        if q1 == 0:
            chqt = 1
            shqtoq = t
        elif q1 < 0:
            q = math.sqrt(-q1)
            chqt = math.cos(q*t)
            shqtoq = math.sin(q*t) / q
        else:
            q = math.sqrt(q1)
            chqt = math.cosh(q*t)
            shqtoq = math.sinh(q*t) / q

        eat = numpy.zeros((2, 2))
        eat[0][0] = chqt + st * shqtoq
        eat[0][1] = a2 * shqtoq
        eat[1][0] = a3 * shqtoq
        eat[1][1] = chqt - st * shqtoq
        eat *= math.exp(s*t)

        return eat

    def _analytical_rs(self, t):
        if abs(self._A[0][0]*self._A[1][1] -
               self._A[1][0]*self._A[0][1]) < 1e-8:  # singular
            return self._B*t + self._x0

        eat = self.my_expm(t)
        # eat = scipy.linalg.expm(numpy.array(self._A)*t)
        return (numpy.dot(self._A_inv,
                          numpy.dot(eat - numpy.identity(2), self._B))
                + numpy.dot(eat, self._x0))

    def _an_rs_separate_1(self, eat=None, t=None):
        if self._A_inv is None:
            return self._B[0]*t + self._x0[0]
        if eat is None:
            eat = self.my_expm(t)
        return (eat[0][0]*self._x0[0] + eat[0][1]*self._x0[1] -
                self._A_inv[0][0]*self._B[0] - self._A_inv[0][1]*self._B[1] +
                self._B[0]*(self._A_inv[0][0]*eat[0][0] +
                            self._A_inv[0][1]*eat[1][0]) +
                self._B[1]*(self._A_inv[0][0]*eat[0][1] +
                            self._A_inv[0][1]*eat[1][1]))


    def _an_rs_separate_2(self, eat=None, t=None):
        if self._A_inv is None:
            return self._B[1]*t + self._x0[1]
        if eat is None:
            eat = self.my_expm(t)
            # eat = scipy.linalg.expm(numpy.array(self._A)*t)
        return (eat[1][0]*self._x0[0] + eat[1][1]*self._x0[1] -
                self._A_inv[1][0]*self._B[0] - self._A_inv[1][1]*self._B[1] +
                self._B[0]*(self._A_inv[1][0]*eat[0][0] +
                            self._A_inv[1][1]*eat[1][0]) +
                self._B[1]*(self._A_inv[1][0]*eat[0][1] +
                            self._A_inv[1][1]*eat[1][1]))

    # API functions
    def predict_time(self, x):
        """Predicts time of achiving state `x' if it is possible before state
        change.

        """
        if True in self.change or self._A is None or self._t <= 0:
            an = self.set_analytical_params()
            try:
                self._A_inv = self._A_inv_func()
            except numpy.linalg.linalg.LinAlgError:
                return self._gtoc_singular(xval=[x[an[0]], x[an[1]]])
        if self._A_inv is None:
            return self._gtoc_singular(xval=[x[an[0]], x[an[1]]])

        return [self.get_time_of_change_1(xval=x[an[0]]),
                self.get_time_of_change_2(xval=x[an[1]])]

    def update_params(self):
        if True in self.change or self._A is None or self._t <= 0:
            self.set_analytical_params()
            try:
                self._A_inv = self._A_inv_func()
            except numpy.linalg.linalg.LinAlgError:
                pass

    def step_to_change(self):
        tries = None
        if True in self.change or self._A is None or self._t <= 0:
            an = self.set_analytical_params()
            try:
                self._A_inv = self._A_inv_func()
            except numpy.linalg.linalg.LinAlgError:
                tries = self._gtoc_singular()

        self.change = [False, False]
        if self._A_inv is None:
            tries = self._gtoc_singular()
        else:
            tries = [self.get_time_of_change_1(),
                     self.get_time_of_change_2()]

        if tries[0] is not None and self.tmax > tries[0] > 0 and (
                tries[1] is None or tries[0] <= tries[1] or tries[1] <= 0
                ) and (self._an_rs_separate_2(t=tries[0]) >
                       self._an_rs_separate_2(t=0)):
            # use tries[0]
            tau = tries[0]
            self.active[0], self.active[1] = self.active[1], self.active[0]
            self._x[0] = self._x[1] = 0.
            est_x = self._an_rs_separate_2(t=tau)
            # print('1: ', est_x)
            self._x[an[1]] = est_x if est_x < 1 else 0.9999999
            self.change[0] = True
        elif tries[1] is not None and self.tmax > tries[1] > 0 and (
                tries[0] is None or tries[1] <= tries[0] or tries[0] <= 0
                ) and (self._an_rs_separate_1(t=tries[1]) >
                       self._an_rs_separate_1(t=0)):
            # use tries[1]
            tau = tries[1]
            self.active[2], self.active[3] = self.active[3], self.active[2]
            self._x[2] = self._x[3] = 0.
            est_x = self._an_rs_separate_1(t=tau)
            # print('2: ', est_x)
            self._x[an[0]] = est_x if est_x < 1 else 0.9999999
            self.change[1] = True
        else:
            # print('ololobad', tries, self._x)
            tau = -1

        if tau > 0:
            self._t = 0

        return tau

    def step(self):
        if True in self.change or self._A is None or self._t <= 0:
            an = self.set_analytical_params()
            try:
                self._A_inv = self._A_inv_func()
            except numpy.linalg.linalg.LinAlgError:
                self._A_inv = None
        else:
            an = self.get_active_numbers()

        self.change = [False]*2
        self._t += self.dt

        x_new = list(self._x)
        x_new[an[0]], x_new[an[1]] = self._analytical_rs(self._t)

        crossed = [i for i in range(4) if self.active[i] and x_new[i] >= 1]

        for i in crossed:
            id1 = (i // 2) * 2
            id2 = (i // 2) * 2 + 1
            self.active[id1], self.active[id2] = (
                self.active[id2], self.active[id1])
            assert self.active[id1] != self.active[id2]
            self._x = x_new
            self._x[id1] = self._x[id2] = 0.
            self.change[i // 2] = True
            self._t = 0.

        return list(x_new)

    def x():
        doc = "Current state of x"

        def fget(self):
            if True in self.change or self._A is None or self._t <= 0:
                return list(self._x)
            x = list(self._x)
            an = self.get_active_numbers()
            x[an[0]], x[an[1]] = self._analytical_rs(self._t)
            return x

        def fset(self, value):
            self._x = list(value)
            self.set_analytical_params()
            try:
                self._A_inv = self._A_inv_func()
            except numpy.linalg.linalg.LinAlgError:
                self._A_inv = None
            self._t = 0.

        return locals()
    x = property(**x())
