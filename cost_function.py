#!/usr/bin/env python3
import math
import numpy


HALBERTSMA_MIN = 0.5
HALBERTSMA_MAX = 2.
HALBERTSMA_MIN_SPEED = 0.111  # goslow_fcd2speed(HALBERTSMA_MAX)
HALBERTSMA_MAX_SPEED = 1.155  # goslow_fcd2speed(HALBERTSMA_MIN)

ERROR_MULTIPLIER = 1


def US(N=6):
    return numpy.linspace(HALBERTSMA_MIN_SPEED, HALBERTSMA_MAX_SPEED, N)


################ Formulas
def halbertsma_x1_sw(full_cycle_duration):  # extensor
    return -0.168 + 0.9062*full_cycle_duration


def halbertsma_x2_su(full_cycle_duration):  # flexor
    return 0.168 + 0.0938*full_cycle_duration


def goslow_fcd2speed(full_cycle_duration):
    return (full_cycle_duration / 0.5445) ** (-1.6877)


def goslow_speed2fcd(speed):
    return 0.5445 * speed**(-0.5925)


def goslow_speed2fcd_difference_param(left2right):
    return left2right ** (-0.5925)


def goslow_fcd2speed_difference_param(left2right):
    return left2right ** (-1.6877)


def halbertsma_minmax_distance(full_cycle_duration):
    raise DeprecationWarning
    if full_cycle_duration < HALBERTSMA_MIN:
        return 1. - full_cycle_duration / HALBERTSMA_MIN
    if full_cycle_duration > HALBERTSMA_MAX:
        return full_cycle_duration / HALBERTSMA_MAX - 1.
    return 0.


################ Cost functions
def cycle_durations(periods):
    fcds = []  # full cycle durations
    for period in periods:
        if period is None or any(j < 0 for j in period):
            fcds.append(None)
        else:
            fcds.append([period[0] + period[1], period[2] + period[3]])
    return fcds


def halbertsma_diff(periods, fcds):
    """Difference from Halbertsma"""
    answ1 = 0.
    answ2 = 0.
    for i, period in enumerate(periods):
        if period is None or any(j < 0 for j in period):
            answ1 += 1.
            answ2 += 1.
            continue
        x1_biol = halbertsma_x1_sw(fcds[i][0])  # extensor
        x2_biol = halbertsma_x2_su(fcds[i][0])  # flexor
        answ1 += (period[1] / x1_biol - 1.)**2
        answ2 += (period[0] / x2_biol - 1.)**2
    answ = (math.sqrt(answ1) + math.sqrt(answ2)) / (2. * len(periods))

    answ1 = 0.
    answ2 = 0.
    for i, period in enumerate(periods):
        if period is None or any(j < 0 for j in period):
            answ1 += 1.
            answ2 += 1.
            continue
        x3_biol = halbertsma_x1_sw(fcds[i][1])  # extensor
        x4_biol = halbertsma_x2_su(fcds[i][1])  # flexor
        answ1 += (period[3] / x3_biol - 1.)**2
        answ2 += (period[2] / x4_biol - 1.)**2
    answ += (math.sqrt(answ1) + math.sqrt(answ2)) / (2. * len(periods))

    return answ


def range_fullfillment(fcds):
    """Makes produced periods fit into the minmaxes."""
    answ = 0.
    fcds_no_none = [i for i in fcds if i is not None]
    answ = float(len(fcds) - len(fcds_no_none))
    if len(fcds_no_none) > 0:
        answ += (min(i[0] for i in fcds_no_none) / HALBERTSMA_MIN - 1.)**2
        answ += (min(i[1] for i in fcds_no_none) / HALBERTSMA_MIN - 1.)**2
        answ += (max(i[0] for i in fcds_no_none) / HALBERTSMA_MAX - 1.)**2
        answ += (max(i[1] for i in fcds_no_none) / HALBERTSMA_MAX - 1.)**2
    return answ


def ffee_crossing(overlaps, fcds):
    """Crossing of flexor/flexor extensor/extensor"""
    answ = 0.

    for overlap, fcd in zip(overlaps, fcds):
        if overlap is None or any(j[1] < 0 for j in overlap.items()):
            answ += 1.
            continue
        answ += (1*overlap['ff'] / sum(fcd)) if sum(fcd) > 0 else 0.
        answ += (0*overlap['ee'] / sum(fcd)) if sum(fcd) > 0 else 0.

    answ /= len(overlaps)

    return answ


def leftright_speed_differences(periods, fcds, left2right=1.):
    assert left2right > 0
    left2right_periods = goslow_speed2fcd_difference_param(left2right)

    answ = 0.
    for i, period in enumerate(periods):
        if (period is None or any(j < 0 for j in period) or fcds[i][1] <= 0):
            answ += 1.
            continue
        answ += (1. - fcds[i][0] / fcds[i][1] / left2right_periods)**2

    return answ


def calculate_relative_speed(periods, fcds):
    persdiff = []
    j = 0
    for i, period in enumerate(periods):
        if (period is None or any(j < 0 for j in period) or fcds[i][1] <= 0):
            continue
        j += 1
        persdiff.append(fcds[i][0] / fcds[i][1])
    answ = goslow_fcd2speed_difference_param(sum(persdiff) / j)
    # if abs(answ - 1.1243759376777638) < 1e-8:
    #     print('\n\n', persdiff, j, '\n\n')
    return answ


ERROR_CALCULATIONS = ('minmax', 'bio', 'ffee', 'lrspeeddiff')


def cost_function(periods, overlaps,
                  multiplier=ERROR_MULTIPLIER,
                  verbose=False, err_distrib=None):
    if periods is None or len(periods) < 1:
        return err_multiplier

    fcds = cycle_durations(periods)

    minmax = range_fullfillment(fcds)
    bio = halbertsma_diff(periods, fcds)
    ffee = ffee_crossing(overlaps, fcds)
    lrspeeddiff = leftright_speed_differences(periods, fcds, left2right=1.)

    # print periods, minmax, bio, ffee

    total = (minmax * 0.7 +
             bio * 1
             # - ffee * 1
             + ffee * 2
             + lrspeeddiff * 0.4
             ) * multiplier
    if verbose:
        print('errors: minmax:{:.4f} bio:{:.4f} '
              'ffee:{:.4f} lrspeeddiff:{:.4f} total:{:.4f}'.format(
                  minmax, bio, ffee, lrspeeddiff, total))

    if err_distrib is not None:
        err_distrib[0] = minmax
        err_distrib[1] = bio
        err_distrib[2] = ffee
        err_distrib[3] = lrspeeddiff
        err_distrib.append(calculate_relative_speed(periods, fcds))

    return total
