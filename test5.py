import random
import pylab
import sys


def main():
    N = 1000

    st1 = []
    st2 = []
    res = []
    for i in range(N):
        st1.append([False]*4)
        st1[-1][random.randint(0, 1)] = st1[-1][random.randint(2, 3)] = True

        st2.append(list(st1[-1]))
        ch = random.randint(0, 1)
        st2[-1][ch*2] = not st2[-1][ch*2]
        st2[-1][ch*2+1] = not st2[-1][ch*2+1]

        try:
            res.append(
                [ai and (ai != bi)
                 for ai, bi in zip(st1[-1], st2[-1])].index(True))
        except:
            print(st1[-1], st2[-1], ch, st2[-1][ch*2], not st2[-1][ch*2])
            sys.exit()

    pylab.figure()
    bins = [-0.5, 0.5, 1.5, 2.5, 3.5]
    pylab.hist(res, bins=bins)


if __name__ == '__main__':
    main()

    pylab.show()
