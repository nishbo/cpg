#!/usr/bin/env python3
import math
import sys
import os
import random
import pickle

from multiprocessing import Process, Array, Value, Pool
from functools import partial
from copy import deepcopy

import numpy
import scipy.optimize
try:
    import pylab
    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d import axes3d
    from matplotlib import cm
    from matplotlib import colors
except:
    pylab = None
import nlopt

import argparse

from simsimpy import other
from simsimpy.subset import SubsetStorage
import simsimpy as ssp

import cpgpy
import simulator
import cost_function
# import output
# import symmetrical
# import search_wrappers
# from optim import *
# from walk_search import *
import meta

import main


def main():
    parser = argparse.ArgumentParser(
        description='Make a plot for a specific point and configuration.')
    # CPG configuration
    parser.add_argument('-n', '--numerical',
                        dest='n', type=float, default=-1,
                        help='Specifies numerical integration step.'
                        ' If <=0, analytical solutions will be used.'
                        ' Defaults to -1.')

    parser.add_argument('-d', '--decouple-function',
                        dest='d', type=str, default=None,
                        help='Decoupling function from main.'
                        'ref rfe ree guf gue gu')
    parser.add_argument('-dv', '--decouple-value',
                        dest='dv', type=float, default=0,
                        help='Percent value for assymmetric decoupling.'
                        'Will be explored left=1-dv, right=1+dv; and'
                        'left=1+dv, right=1-dv')

    parser.add_argument('-r', '--plot-ruler',
                        dest='r', action='store_true',
                        help='plots a ruler')
    parser.add_argument('-p', '--plot-period',
                        dest='p', action='store_true',
                        help='plots produced periods vs total step cycle.')
    parser.add_argument('-i', '--plot-speedvsinput',
                        dest='i', action='store_true',
                        help='plots produced speeds vs input.')

    args = parser.parse_args()


    pass


if __name__ == '__main__':
    main()
