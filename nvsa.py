# MB OBSOLETE:
# dts:         [0.001, 0.0003, 9e-05, 2.7e-05, 8.1e-06, 2.43e-06, 7.29e-07,
# 2.187e-07, 6.561e-08, 1.9683e-08]
# differences: [3.225094520368575e-11, 2.9024954036716984e-12,
# 2.6081834283572177e-13, 1.2759760102707244e-14, 2.1114885578186507e-15,
# 1.8894919181985179e-16, 1.9477458035863977e-17, 3.457585460428473e-18,
# 3.3145473035979053e-20, 1.9682999700551845e-08]


from multiprocessing import Process, Array, Value
import sys
import os
import numpy
try:
    import pylab
except:
    pylab = None
import time
try:
    import cPickle as pickle
except:
    import pickle

from simsimpy import other
import cpgpy
import symmetrical

FIGARGS = {
    'figsize': (9., 9.),
    'dpi': 100
}


def run_test7_p(dt, us, an, periods1, periods2, periods3, periods4,
                preset=None):
    """Used in test7 and get_periods_a. Extracts periods from any given model.

    preset is used to supply a preset code or preset=[point, description]
    """
    # an= 'a' analytical, 'n' numerical
    if an == 'a':
        cpg = cpgpy.Cpg_Analytical()
    elif an == 'n':
        cpg = cpgpy.Cpg_Numerical()
    else:
        raise ValueError

    sim = cpgpy.Simulator(cpg)
    sim.x_start = [0., 0., 0., 0.]
    sim.us = us
    sim.dt = dt

    if preset is None:
        sim.cpg.set_preset('sergiy_sym')
    elif isinstance(preset, str):
        sim.cpg.set_preset(preset)
    elif isinstance(preset, (list, tuple)):
        sim.set_point(preset[0], preset[1])

    p = sim.get_periods_overlaps_ladder(verbose=True)
    for i in xrange(len(us)):
        periods1[i] = p[i][0]
        periods2[i] = p[i][1]
        periods3[i] = p[i][2]
        periods4[i] = p[i][3]

    return [periods1, periods2, periods3, periods4]


def get_periods_a(us, dt, preset=None):
    """Extracts periods from and analytical CPG model."""
    periods1_a = numpy.zeros(len(us))
    periods2_a = numpy.zeros(len(us))
    periods3_a = numpy.zeros(len(us))
    periods4_a = numpy.zeros(len(us))

    run_test7_p(dt, us, 'a',
                periods1_a, periods2_a,
                periods3_a, periods4_a,
                preset=preset)

    return periods1_a, periods2_a, periods3_a, periods4_a


def test7_p():
    """Compares numerical and analytical solution for the CPG model.

    Result: success.
    """
    dts = [i for i in other.dlogrange(1e-1, 0.3, stop=1e-4)]
    us = [i for i in other.dxrange(1., 4, 0.5)]
    procs = []

    periods1_n = []
    periods2_n = []
    periods3_n = []
    periods4_n = []

    for dt in dts:
        periods1_n.append(Array('d', len(us)))
        periods2_n.append(Array('d', len(us)))
        periods3_n.append(Array('d', len(us)))
        periods4_n.append(Array('d', len(us)))
        procs.append(Process(
            target=run_test7_p,
            args=(dt, us, 'n',
                  periods1_n[-1], periods2_n[-1],
                  periods3_n[-1], periods4_n[-1],
                  [SYM_POINT, symmetrical.DESCRIPTION])))
        procs[-1].start()
    print '{} major processes started.'.format(len(procs)+1)

    periods1_a, periods2_a, periods3_a, periods4_a = get_periods_a(us, 1e-9)

    # print procs
    for proc in procs:
        # print proc
        proc.join()

    periods1_n = [numpy.array(i) for i in periods1_n]
    periods2_n = [numpy.array(i) for i in periods2_n]
    periods3_n = [numpy.array(i) for i in periods3_n]
    periods4_n = [numpy.array(i) for i in periods4_n]

    with open('smth.pcl', 'w') as f:
        pickle.dump([dts, us, [periods1_a, periods2_a, periods3_a, periods4_a],
                     [periods1_n, periods2_n, periods3_n, periods4_n]], f)

    if pylab is not None:
        plot_test7()


def plot_test7(fname='smth'):
    if pylab is None:
        return None

    with open(fname + '.pcl') as f:
        dts, us, periodsa, periodsn = pickle.load(f)

    for k in xrange(4):
        pylab.figure(**FIGARGS)
        for i, u in enumerate(us):
            if u > 4:
                break
            # if u > 3.1 or u < 2.9:
            #     continue
            pylab.subplot(2, 1, 1)
            pylab.semilogx(
                dts, [abs(j[i] - periodsa[k][i])/periodsa[k][i]
                      for j in periodsn[k]],
                label=str(u))
            pylab.xlim([0.2, 1e-4])
            pylab.ylabel('relative diff b/w an and num, %% of an')
            pylab.legend()

            pylab.subplot(2, 1, 2)
            pylab.semilogx(dts, [j[i] for j in periodsn[k]], label=str(u)+'n')
            pylab.semilogx(dts, [periodsa[k][i]]*len(dts), label=str(u)+'a')
            pylab.xlim([0.2, 1e-4])
            pylab.xlabel('dt')
            pylab.ylabel('periods')
            pylab.legend()
        pylab.savefig(fname + str(k+1) + '.png')
    pylab.show()


def test8(an, dt):
    # an= 'a' analytical, 'n' numerical
    if an == 'a':
        cpg = cpgpy.Cpg_Analytical()
    elif an == 'n':
        cpg = cpgpy.Cpg_Numerical()
    else:
        raise ValueError
    sim = cpgpy.Simulator(cpg)
    sim.us = [2.]
    sim.x_start = [0.]*4
    sim.dt = dt

    sim.set_point(SYM_POINT, symmetrical.DESCRIPTION)

    sim.cpg.x = list(sim.x_start)
    sim.cpg.u = [2.]*4
    return sim.run(5.)


SYM_POINT = [0.6369, 0.5542, 0.5809, 0.0, 1.9456, 0.5869, 0.3553, -0.457,
             -0.04, 0.1015, 2.8904]


next0 = lambda x, shift: shift+1+x[shift+1:].index(0)


def sten(x):
    try:
        istart = x.index(0)
    except ValueError:
        return [0, len(x) - 1]
    while istart + 1 == next0(x, istart):
        istart = next0(x, istart)
    iend = next0(x, istart)
    print istart, iend
    return [istart, iend]


def plot_test8():
    if pylab is None:
        return None

    pylab.figure(**FIGARGS)
    t, x = test8('a', 1e-3)
    pylab.subplot(2, 1, 1)
    istart, iend = sten(x[0])
    pylab.plot([i - t[istart] for i in t[istart:iend]], x[0][istart:iend],
               label='a x1')
    print '\tx1t end', t[iend] - t[istart]
    # istart, iend = sten(x[1])
    # pylab.plot([i - t[istart] for i in t[istart:iend]], x[1][istart:iend],
    #            label='a x2')
    pylab.subplot(2, 1, 2)
    # istart, iend = sten(x[2])
    # pylab.plot([i - t[istart] for i in t[istart:iend]], x[2][istart:iend],
    #            label='a x3')
    istart, iend = sten(x[3])
    pylab.plot([i - t[istart] for i in t[istart:iend]], x[3][istart:iend],
               label='a x4')
    print '\t\tx4t end', t[iend] - t[istart]
    for dt in other.dlogrange(1e-1, 0.3, stop=1e-5):
        t, x = test8('n', dt)
        pylab.subplot(2, 1, 1)
        istart, iend = sten(x[0])
        pylab.plot([i - t[istart] for i in t[istart:iend]], x[0][istart:iend],
                   label='n x1 {}'.format(dt))
        print '\tx1t end', t[iend] - t[istart]
        # istart, iend = sten(x[1])
        # pylab.plot([i - t[istart] for i in t[istart:iend]],
        #            x[1][istart:iend],
        #            label='n x2 {}'.format(dt))
        pylab.ylim([-1e-4, 1.])
        pylab.legend()
        pylab.subplot(2, 1, 2)
        # istart, iend = sten(x[2])
        # pylab.plot([i - t[istart] for i in t[istart:iend]],
        #            x[2][istart:iend],
        #            label='n x3 {}'.format(dt))
        istart, iend = sten(x[3])
        pylab.plot([i - t[istart] for i in t[istart:iend]], x[3][istart:iend],
                   label='n x4 {}'.format(dt))
        print '\t\tx4t end', t[iend] - t[istart]
        pylab.ylim([-1e-4, 1.])
        pylab.legend()
    pylab.savefig('well.png')

    print 'numerical', run_test7_p(1e-4, [2.], 'n', [0.], [0.], [0.], [0.],
                                   preset=(SYM_POINT, symmetrical.DESCRIPTION))
    print 'analytical', run_test7_p(1e-8, [2.], 'a', [0.], [0.], [0.], [0.],
                                    preset=(SYM_POINT,
                                            symmetrical.DESCRIPTION))

    pylab.show()


def test9():
    print 'numerical'
    print run_test7_p(
        1e-4, [2.], 'n', [0.], [0.], [0.], [0.],
        [SYM_POINT, symmetrical.DESCRIPTION])
    print 'analytical'
    print run_test7_p(
        1e-4, [2.], 'a', [0.], [0.], [0.], [0.],
        [SYM_POINT, symmetrical.DESCRIPTION])


def test10_run(an, dt, u, N, starts_in=None, starts_out=None):
    if an == 'a':
        cpg = cpgpy.Cpg_Analytical()
    elif an == 'n':
        cpg = cpgpy.Cpg_Numerical()
    else:
        raise ValueError

    point = symmetrical.TEST_POINT
    descr = symmetrical.DESCRIPTION

    sim = cpgpy.Simulator(cpg)
    sim.x_start = [0., 0., 0., 0.]
    sim.set_point(point, descr)
    sim.cpg.x = [point[0], 0., 0., point[1]]
    sim.cpg.u = [u]*4

    taus = []
    t = time.time()
    for i in xrange(N):
        print 'st:', time.time()
        taus.append(sim.cpg.step_to_change_timemeasure(dt))
        print 'end:', time.time()
        if starts_out is not None:
            starts_out[i] = list(sim.cpg.x)
        if starts_in is not None:
            sim.cpg.x = list(starts_in[i])
    t -= time.time()
    print -t
    return [taus, -t]


def purrprint(a, rnd=3):
    return [round(i, rnd) for i in a]


def test10():
    u = 1.
    N = 4
    dts = [_ for _ in other.dlogrange(1e-2, .1, stop=1e-5)]
    # dts = [_ for _ in other.dlogrange(1e-2, 1.-2**(-4), stop=1e-5)]

    starts = [None]*N
    adt = 1e-8
    tausa, timea = test10_run('a', adt, u, N, starts_out=starts)
    tausns = []
    timens = []
    for dt in dts:
        tausn, timen = test10_run('n', dt, u, N, starts_in=starts)
        tausns.append(tausn)
        timens.append(timen)

    print round(timea, 4), adt  # , purrprint(tausa)
    for i, taus in enumerate(tausns):
        print round(timens[i], 4), dts[i]  # , purrprint(taus)

    pylab.figure(**FIGARGS)
    for i in xrange(N):
        pylab.subplot(N, 1, i+1)
        pylab.title('{}'.format(i))
        pylab.semilogx(dts, [tausa[i]]*len(dts), label='a{}'.format(i))
        pylab.semilogx(dts, [j[i] for j in tausns], label='n{}'.format(i))
        pylab.xlim([max(dts)*1.4, min(dts)*0.4])
        pylab.xlabel('dts, s')
        pylab.ylabel('taus, s')
    pylab.legend()

    # pylab.figure(**FIGARGS)
    # pylab.semilogx(dts, [sum(abs(j[i]-tausa[i])
    #                          for i in xrange(N))
    #                      for j in tausns], label='SUMn-a')
    # pylab.xlabel('dts, s')
    # pylab.ylabel('difference between analytical and numerical solution, s')
    # pylab.legend()
    # pylab.xlim([max(dts)*1.4, min(dts)*0.4])

    pylab.show()


def test11():
    data = [
        [0.004, 0.01], [0.004, 0.009375], [0.003, 0.0087890625],
        [0.003, 0.00823974609375], [0.004, 0.00772476196289],
        [0.004, 0.00724196434021], [0.005, 0.00678934156895],
        [0.006, 0.00636500772089], [0.006, 0.00596719473833],
        [0.005, 0.00559424506719], [0.007, 0.00524460475049],
        [0.006, 0.00491681695358], [0.006, 0.00460951589398],
        [0.006, 0.00432142115061], [0.011, 0.0040513323287],
        [0.009, 0.00379812405815], [0.012, 0.00356074130452],
        [0.012, 0.00333819497299], [0.011, 0.00312955778717],
        [0.01, 0.00293396042548], [0.011, 0.00275058789888],
        [0.015, 0.0025786761552], [0.013, 0.0024175088955],
        [0.014, 0.00226641458953], [0.015, 0.00212476367769],
        [0.024, 0.00199196594783], [0.018, 0.00186746807609],
        [0.019, 0.00175075132134], [0.017, 0.00164132936375],
        [0.023, 0.00153874627852], [0.025, 0.00144257463611],
        [0.022, 0.00135241372135], [0.022, 0.00126788786377],
        [0.032, 0.00118864487228], [0.045, 0.00111435456777],
        [0.034, 0.00104470740728], [0.037, 0.000979413194325],
        [0.037, 0.00091819986968], [0.034, 0.000860812377825],
        [0.045, 0.000807011604211], [0.042, 0.000756573378948],
        [0.048, 0.000709287542764], [0.043, 0.000664957071341],
        [0.054, 0.000623397254382], [0.055, 0.000584434925983],
        [0.076, 0.000547907743109], [0.064, 0.000513663509165],
        [0.08, 0.000481559539842], [0.074, 0.000451462068602],
        [0.066, 0.000423245689314], [0.064, 0.000396792833732],
        [0.065, 0.000371993281624], [0.077, 0.000348743701522],
        [0.084, 0.000326947220177], [0.083, 0.000306513018916],
        [0.099, 0.000287355955234], [0.119, 0.000269396208032],
        [0.143, 0.00025255894503], [0.14, 0.000236774010966],
        [0.139, 0.000221975635281], [0.141, 0.000208102158076],
        [0.146, 0.000195095773196], [0.177, 0.000182902287371],
        [0.18, 0.00017147089441], [0.173, 0.000160753963509],
        [0.169, 0.00015070684079], [0.228, 0.000141287663241],
        [0.227, 0.000132457184288], [0.235, 0.00012417861027],
        [0.249, 0.000116417447128], [0.308, 0.000109141356683],
        [0.247, 0.00010232002189], [0.271, 9.5925020522e-05],
        [0.34, 8.9929706739e-05], [0.391, 8.4309100068e-05],
        [0.414, 7.9039781314e-05], [0.368, 7.4099794982e-05],
        [0.441, 6.9468557796e-05], [0.474, 6.5126772934e-05],
        [0.488, 6.1056349626e-05], [0.483, 5.7240327774e-05],
        [0.622, 5.3662807288e-05], [0.645, 5.0308881832e-05],
        [0.644, 4.7164576718e-05], [0.758, 4.4216790673e-05],
        [0.715, 4.1453241256e-05], [0.874, 3.8862413677e-05],
        [0.81, 3.6433512822e-05], [0.894, 3.4156418271e-05],
        [0.991, 3.2021642129e-05], [0.955, 3.0020289496e-05],
        [1.175, 2.8144021403e-05], [1.128, 2.6385020065e-05],
        [1.195, 2.4735956311e-05], [1.35, 2.3189959042e-05],
        [1.468, 2.1740586602e-05], [1.531, 2.0381799939e-05],
        [1.517, 1.9107937443e-05], [1.734, 1.7913691353e-05],
        [1.896, 1.6794085643e-05], [1.966, 1.574445529e-05],
        [2.105, 1.4760426834e-05], [2.274, 1.3837900157e-05],
        [2.306, 1.2973031397e-05], [2.633, 1.2162216935e-05],
        [2.58, 1.1402078377e-05], [2.968, 1.0689448478e-05],
        [3.165, 1.0021357948e-05]]
    an_time = 0.222
    data_time = [i[0] for i in data]
    data_precision = [i[1] for i in data]

    pylab.figure(**FIGARGS)
    pylab.loglog(data_precision, [an_time]*len(data_precision))
    pylab.loglog(data_precision, data_time)
    pylab.xlim([max(data_precision)*1.4, min(data_precision)*0.4])
    pylab.xlabel('Precision, s')
    pylab.ylabel('Runtime, s')
    pylab.show()


if __name__ == '__main__':
    # If the problem is in the evolution before the beginning of the step,
    # start with the beginning. Do not use extracting algorythms, use
    # straightforward assign of x0 and measure the step.
    print 'Start Pid: {}, Time: {}'.format(os.getpid(), other.time_stamp())
    if pylab is None:
        sys.stdout = open('log.txt', 'a')
    else:
        pylab.close('all')

    # test7_p()
    # plot_test8()
    # test9()
    # test10()
    test10()

    print 'Finish Pid: {}, Time: {}'.format(os.getpid(), other.time_stamp())
